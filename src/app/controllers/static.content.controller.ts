import {Injectable} from "@angular/core";
import {AppLoggerFactory} from "../utilities/app.logger.factory";
import {LegalRegulationsService} from "../services/moderation/legal.regulations.service";
import {ContentModerationService} from "../services/moderation/content.moderation.service";
import {AboutUs} from "../model/moderation/about.us";
import {AboutUsContentEvent} from "../model/events/about.us.content.event";
import {StaticContentType} from "../model/enum/static.content.type";
import {Faq} from "../model/moderation/faq";
import {P2PAbout} from "../model/moderation/p2p.about";
import {BorrowGuide} from "../model/moderation/borrow.guide";
import {InvestGuide} from "../model/moderation/invest.guide";
import {Contact} from "../model/moderation/contact";
import {LegalRegulation} from "../model/moderation/legal.regulation";
import {FaqContentEvent} from "../model/events/faq.content.event";

@Injectable()
export class StaticContentPageController {

  private _logger = this._loggerFactory.getLogger('StaticContentPageController');

  private viewChangeCallback: (viewChangeEvent) => void;

  private faqViewChangeCallback: (viewChangeEvent) => void;

  public constructor(private _loggerFactory: AppLoggerFactory, private legalRegulationsService: LegalRegulationsService,
                     private contentModeratedService: ContentModerationService) {
    this._logger.logInfo('controller created');
  }

  public subscribeForViewChangeEvents(viewChangeCallback: (viewChangeEvent) => void) {
    this.viewChangeCallback = viewChangeCallback;
    this.contentModeratedService.getAboutUsContent((aboutUs: AboutUs) => {
      this.viewChangeCallback(new AboutUsContentEvent(aboutUs));
    });
  }

  public subscribeForFaqContentViewChangeEvents(viewChangeCallback: (viewChangeEvent) => void) {
    this.faqViewChangeCallback = viewChangeCallback;
    this.contentModeratedService.getFaqContent((faqs: Faq[]) => {
      this.faqViewChangeCallback(new FaqContentEvent(faqs));
    });
  }

  public getAboutUsContent(callback: (aboutUs: AboutUs) => void): void {
    this.contentModeratedService.getAboutUsContent(callback);
  }

  public getP2PAboutContent(callback: (p2pAbout: P2PAbout) => void): void {
    this.contentModeratedService.getP2PAboutContent(callback);
  }

  public getBorrowGuideContent(callback: (guide: BorrowGuide) => void): void {
    this.contentModeratedService.getBorrowGuideContent(callback);
  }

  public getInvestGuideContent(callback: (guide: InvestGuide) => void): void {
    this.contentModeratedService.getInvestGuideContent(callback);
  }

  public getContactContent(callback: (contact: Contact) => void): void {
    this.contentModeratedService.getContactContent(callback);
  }

  public getLegalRegulationContent(id: string, callback: (regulation: LegalRegulation) => void): void {
    this.legalRegulationsService.getRegulationText(id, callback);
  }

}
