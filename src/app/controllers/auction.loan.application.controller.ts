import {Injectable} from "@angular/core";
import {AppLoggerFactory} from "../utilities/app.logger.factory";
import {User} from "../model/user/User";
import {ApplicationDataSynchronizationService} from "../services/application.data.synchronization.service";
import {Auction} from "../model/auction/auction";
import {AuctionsService} from "../services/auctions.service";
import {Router} from "@angular/router";

@Injectable()
export class AuctionLoanApplicationPageController {

  private _logger = this._loggerFactory.getLogger('AuctionLoanApplicationPageController');

  private loggedInUser = new User();

  private viewChangeCallback: (viewChangeEvent) => void;

  public constructor(private dataSharedService: ApplicationDataSynchronizationService,
                     private auctionsService: AuctionsService, private router: Router,
                     private _loggerFactory: AppLoggerFactory) {
    this.loggedInUser = dataSharedService.loggedInUser;
    dataSharedService.loggedInUserSubject.subscribe((loggedInUser: User) => {
      this.loggedInUser = loggedInUser;
    });
    this._logger.logInfo('controller created');
  }

  public subscribeForViewChangeEvents(viewChangeCallback: (viewChangeEvent) => void) {
    this.viewChangeCallback = viewChangeCallback;
  }

  public saveNewAuction(auction: Auction, saveCallback: (isSaved: boolean) => void): void {
    this.auctionsService.saveAuction(this.loggedInUser, auction, saveCallback);
  }

}
