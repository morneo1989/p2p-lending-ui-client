import {Injectable} from "@angular/core";
import {AppLoggerFactory} from "../utilities/app.logger.factory";
import {LegalRegulationsService} from "../services/moderation/legal.regulations.service";
import {User} from "../model/user/User";
import {LegalRegulation} from "../model/moderation/legal.regulation";
import {ApplicationDataSynchronizationService} from "../services/application.data.synchronization.service";
import {UserType} from "../model/user/enums/user.type";
import {LegalRegulationTitlesEvent} from "../model/events/legal.regulation.titles.event";
import {ContentModerationService} from "../services/moderation/content.moderation.service";
import {AboutUs} from "../model/moderation/about.us";
import {AboutUsContentEvent} from "../model/events/about.us.content.event";
import {BorrowGuide} from "../model/moderation/borrow.guide";
import {BorrowGuideContentEvent} from "../model/events/borrow.guide.content.event";
import {InvestGuide} from "../model/moderation/invest.guide";
import {Contact} from "../model/moderation/contact";
import {P2PAbout} from "../model/moderation/p2p.about";
import {Faq} from "../model/moderation/faq";
import {InvestGuideContentEvent} from "../model/events/invest.guide.content.event";
import {P2PAboutContentEvent} from "../model/events/p2p.about.content.event";
import {ContactContentEvent} from "../model/events/contact.content.event";
import {FaqContentEvent} from "../model/events/faq.content.event";

@Injectable()
export class ContentModerationPageController {

  private _logger = this._loggerFactory.getLogger('ContentModerationPageController');

  private loggedInUser = new User();

  private legalRegulationsViewChangeCallback: (viewChangeEvent) => void;

  private aboutUsViewChangeCallback: (viewChangeEvent) => void;

  private borrowGuideViewChangeCallback: (viewChangeEvent) => void;

  private investGuideViewChangeCallback: (viewChangeEvent) => void;

  private p2pAboutViewChangeCallback: (viewChangeEvent) => void;

  private contactViewChangeCallback: (viewChangeEvent) => void;

  private faqViewChangeCallback: (viewChangeEvent) => void;

  public constructor(private dataSharedStorage: ApplicationDataSynchronizationService, private legalRegulationsService: LegalRegulationsService,
                     private contentModerationService: ContentModerationService, private _loggerFactory: AppLoggerFactory) {
    this.loggedInUser = dataSharedStorage.loggedInUser;
    dataSharedStorage.loggedInUserSubject.subscribe((loggedInUser: User) => {
      this._logger.logDebug('logged in user change: ' + JSON.stringify(loggedInUser));
      this.loggedInUser = loggedInUser;
    });
    if(this.legalRegulationsViewChangeCallback != null && this.loggedInUser.token != null && this.loggedInUser.userType == UserType.ADMIN) {
      this.legalRegulationsService.getRegulationsTitlesWithIds((regulationTitles: LegalRegulation[]) => {
        this.legalRegulationsViewChangeCallback(new LegalRegulationTitlesEvent(regulationTitles));
      });
    }
    this._logger.logInfo('controller created');
  }

  public subscribeForLegalRegulationsViewChangeEvents(viewChangeCallback: (viewChangeEvent) => void) {
    this.legalRegulationsViewChangeCallback = viewChangeCallback;
    if(this.loggedInUser.token != null && this.loggedInUser.userType == UserType.ADMIN) {
      this.legalRegulationsService.getRegulationsTitlesWithIds((regulationTitles: LegalRegulation[]) => {
        this.legalRegulationsViewChangeCallback(new LegalRegulationTitlesEvent(regulationTitles));
      });
    }
  }

  public subscribeForAboutUsViewChangeEvents(viewChangeCallback: (viewChangeEvent) => void) {
    this.aboutUsViewChangeCallback = viewChangeCallback;
    if(this.loggedInUser.token != null && this.loggedInUser.userType == UserType.ADMIN) {
      this.contentModerationService.getAboutUsContent((aboutUsContent: AboutUs) => {
        this.aboutUsViewChangeCallback(new AboutUsContentEvent(aboutUsContent));
      });
    }
  }

  public subscribeForBorrowGuideViewChangeEvents(viewChangeCallback: (viewChangeEvent) => void) {
    this.borrowGuideViewChangeCallback = viewChangeCallback;
    if(this.loggedInUser.token != null && this.loggedInUser.userType == UserType.ADMIN) {
      this.contentModerationService.getBorrowGuideContent((borrowGuideContent: BorrowGuide) => {
        this.borrowGuideViewChangeCallback(new BorrowGuideContentEvent(borrowGuideContent));
      });
    }
  }

  public subscribeForInvestGuideViewChangeEvents(viewChangeCallback: (viewChangeEvent) => void) {
    this.investGuideViewChangeCallback = viewChangeCallback;
    if(this.loggedInUser.token != null && this.loggedInUser.userType == UserType.ADMIN) {
      this.contentModerationService.getInvestGuideContent((investGuideContent: InvestGuide) => {
        this.investGuideViewChangeCallback(new InvestGuideContentEvent(investGuideContent));
      });
    }
  }

  public subscribeForP2PAboutViewChangeEvents(viewChangeCallback: (viewChangeEvent) => void) {
    this.p2pAboutViewChangeCallback = viewChangeCallback;
    if(this.loggedInUser.token != null && this.loggedInUser.userType == UserType.ADMIN) {
      this.contentModerationService.getP2PAboutContent((p2pAboutContent: P2PAbout) => {
        this.p2pAboutViewChangeCallback(new P2PAboutContentEvent(p2pAboutContent));
      });
    }
  }

  public subscribeForContactViewChangeEvents(viewChangeCallback: (viewChangeEvent) => void) {
    this.contactViewChangeCallback = viewChangeCallback;
    if(this.loggedInUser.token != null && this.loggedInUser.userType == UserType.ADMIN) {
      this.contentModerationService.getContactContent((contactContent: Contact) => {
        this.contactViewChangeCallback(new ContactContentEvent(contactContent));
      });
    }
  }

  public subscribeForFaqViewChangeEvents(viewChangeCallback: (viewChangeEvent) => void) {
    this.faqViewChangeCallback = viewChangeCallback;
    if(this.loggedInUser.token != null && this.loggedInUser.userType == UserType.ADMIN) {
      this.contentModerationService.getFaqContent((faqs: Faq[]) => {
        this.faqViewChangeCallback(new FaqContentEvent(faqs));
      });
    }
  }

  public getLegalRegulationsTitles(regulationTitlesCallback: (legalRegulations: LegalRegulation[]) => void) {
    this.legalRegulationsService.getRegulationsTitlesWithIds(regulationTitlesCallback);
  }

  public getLegalRegulationText(legalRegulationId, regulationTextCallback: (legalRegulation: LegalRegulation) => void) {
    this.legalRegulationsService.getRegulationText(legalRegulationId, regulationTextCallback);
  }

  public saveLegalRegulation(legalRegulationToSave: LegalRegulation, saveCallback: (isSaved: boolean) => void) {
    this.legalRegulationsService.saveRegulation(legalRegulationToSave, this.loggedInUser, saveCallback);
  }

  public updateLegalRegulation(legalRegulationToUpdate: LegalRegulation, updateCallback: (isSaved: boolean) => void) {
    this.legalRegulationsService.updateRegulation(legalRegulationToUpdate, this.loggedInUser, updateCallback);
  }

  public saveOrUpdateAboutUs(aboutUs: AboutUs, callback: (isDone: boolean) => void) {
    this.contentModerationService.saveOrUpdateAboutUs(aboutUs, this.loggedInUser, callback);
  }

  public saveOrUpdateBorrowGuide(borrowGuide: BorrowGuide, callback: (isDone: boolean) => void) {
    this.contentModerationService.saveOrUpdateBorrowGuide(borrowGuide, this.loggedInUser, callback);
  }

  public saveOrUpdateInvestGuide(investGuide: InvestGuide, callback: (isDone: boolean) => void) {
    this.contentModerationService.saveOrUpdateInvestGuide(investGuide, this.loggedInUser, callback);
  }

  public saveOrUpdateContact(contact: Contact, callback: (isDone: boolean) => void) {
    this.contentModerationService.saveOrUpdateContact(contact, this.loggedInUser, callback);
  }

  public saveOrUpdateP2PAbout(p2pAbout: P2PAbout, callback: (isDone: boolean) => void) {
    this.contentModerationService.saveOrUpdateP2PAbout(p2pAbout, this.loggedInUser, callback);
  }

  public saveOrUpdateFaq(faqs: Faq[], callback: (isDone: boolean) => void) {
    this.contentModerationService.saveOrUpdateFaq(faqs, this.loggedInUser, callback);
  }

}
