import {Injectable} from "@angular/core";
import {AppLoggerFactory} from "../utilities/app.logger.factory";

@Injectable()
export class InvestorPanelPageController {

  private _logger = this._loggerFactory.getLogger('InvestorPanelPageController');

  private viewChangeCallback: (viewChangeEvent) => void;

  public constructor(private _loggerFactory: AppLoggerFactory) {
    this._logger.logInfo("controller created");
  }

  public subscribeForViewChangeEvents(viewChangeCallback: (viewChangeEvent) => void) {
    this.viewChangeCallback = viewChangeCallback;
  }

}
