import {Injectable} from "@angular/core";
import {AppLoggerFactory} from "../utilities/app.logger.factory";
import {ContentModerationService} from "../services/moderation/content.moderation.service";
import {LegalRegulationsService} from "../services/moderation/legal.regulations.service";
import {User} from "../model/user/User";
import {ApplicationDataSynchronizationService} from "../services/application.data.synchronization.service";
import {LegalRegulation} from "../model/moderation/legal.regulation";
import {LegalRegulationTitlesEvent} from "../model/events/legal.regulation.titles.event";

@Injectable()
export class FooterPageController {

  private _logger = this._loggerFactory.getLogger('FooterPageController');

  private loggedInUser: User;

  private viewChangeCallback: (viewChangeEvent) => void;

  public constructor(private _loggerFactory: AppLoggerFactory, private legalRegulationsService: LegalRegulationsService,
                     private dataSharedStorage: ApplicationDataSynchronizationService) {
    this.loggedInUser = dataSharedStorage.loggedInUser;
    dataSharedStorage.loggedInUserSubject.subscribe((loggedInUser: User) => {
      this.loggedInUser = loggedInUser;
    });
    this._logger.logInfo('controller created');
  }

  public subscribeForViewChangeEvents(viewChangeCallback: (viewChangeEvent) => void) {
    this.viewChangeCallback = viewChangeCallback;
    this.legalRegulationsService.getRegulationsTitlesWithIds((regulations: LegalRegulation[]) => {
      this.viewChangeCallback(new LegalRegulationTitlesEvent(regulations));
    });
  }

}
