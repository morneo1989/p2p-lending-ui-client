import {Injectable} from "@angular/core";
import {AppLoggerFactory} from "../utilities/app.logger.factory";
import {User} from "../model/user/User";
import {UsersRegistrationService} from "../services/users.registration.service";
import {PhoneVerifyResponse} from "../model/user/PhoneVerifyResponse";
import {UserRegisterResponse} from "../model/user/UserRegisterResponse";
import {Auction} from "../model/auction/auction";

@Injectable()
export class RegistrationPanelPageController {

  private _logger = this._loggerFactory.getLogger('RegistrationPanelPageController');

  private viewChangeCallback: (viewChangeEvent) => void;

  public constructor(private _loggerFactory: AppLoggerFactory, private registrationService: UsersRegistrationService) {
    this._logger.logInfo('controller created');
  }

  public subscribeForViewChangeEvents(viewChangeCallback: (viewChangeEvent) => void) {
    this.viewChangeCallback = viewChangeCallback;
  }

  public registerBasicBorrower(userToRegister: User, auctionToCreate: Auction, registrationCallback: (response: UserRegisterResponse) => void) {
    this.registrationService.registerBasicBorrower(userToRegister, auctionToCreate, registrationCallback);
  }

  public registerFullBorrower(userToRegister: User, auctionToCreate: Auction, registrationCallback: (response: UserRegisterResponse) => void) {
    this.registrationService.registerFullBorrower(userToRegister, auctionToCreate, registrationCallback);
  }

  public registerBasicInvestor(userToRegister: User, registrationCallback: (response: UserRegisterResponse) => void) {

    this.registrationService.registerBasicInvestor(userToRegister, registrationCallback);
  }

  public registerFullInvestor(userToRegister: User, registrationCallback: (response: UserRegisterResponse) => void) {
    this.registrationService.registerFullInvestor(userToRegister, registrationCallback);
  }

  public sendPhoneVerificationRequest(email: string, phoneNumber: string, requestCallback: (response: PhoneVerifyResponse) => void) {
    this.registrationService.sendPhoneVerificationRequest(email, phoneNumber, requestCallback);
  }

}
