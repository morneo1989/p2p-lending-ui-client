import {Injectable} from "@angular/core";
import {User} from "../model/user/User";
import {Headers, Http, Response} from "@angular/http";
import {Auction} from "../model/auction/auction";
import {UserRegisterResponse} from "../model/user/UserRegisterResponse";
import {PhoneVerifyResponse} from "../model/user/PhoneVerifyResponse";
import {RegistrationUrlConfiguration} from "../configuration/registration.url.configuration";
import {AppLoggerFactory} from "../utilities/app.logger.factory";

@Injectable()
export class UsersRegistrationService {

  private _logger = this._loggerFactory.getLogger('UsersRegistrationService');

  private headersForRequest = new Headers({'Content-Type': 'application/json'});

  constructor(private http: Http, private _loggerFactory: AppLoggerFactory) {
    this._logger.logInfo('service created')
  }

  public sendPhoneVerificationRequest(email: String, phone: String, verificationPhoneSendCallback: (response: PhoneVerifyResponse) => void): void {
    this._logger.logInfo('send phone verification request for email: ' + email + ' and phone number: ' + phone);
    this.http.post(RegistrationUrlConfiguration.PHONE_VERIFICATION, JSON.stringify({
        email: email,
        phone: phone
      }),
      {headers: this.headersForRequest})
      .subscribe((r: Response) => {
        if (r.status == 200) {
          verificationPhoneSendCallback(PhoneVerifyResponse.SUCCESS);
        } else if (r.status == 409) {
          verificationPhoneSendCallback(PhoneVerifyResponse.EMAIL_ALREADY_EXISTS);
        } else {
          verificationPhoneSendCallback(PhoneVerifyResponse.UNKNOWN_ERROR);
        }
      }, (error) => {
        this._logger.logError('Error while sending phone verification request: ' + JSON.stringify(error));
        verificationPhoneSendCallback(PhoneVerifyResponse.UNKNOWN_ERROR);
      });
  }

  public registerBasicBorrower(user: User, auction: Auction, simpleBorrowerRegistrationCallback: (response: UserRegisterResponse) => void): void {
    this._logger.logInfo('register new basic borrower: ' + user + ' with loan application: ' + auction);
    this.http.post(RegistrationUrlConfiguration.BASIC_BORROWER_REGISTRATION, JSON.stringify({
      email: user.email,
      phone: user.phone,
      phoneVerificationCode: user.phoneVerificationCode,
      password: user.password,
      name: user.name,
      surname: user.surname,
      loanApplication: auction
    }), {headers: this.headersForRequest})
      .subscribe((r: Response) => {
        if (r.status == 201) {
          simpleBorrowerRegistrationCallback(UserRegisterResponse.SUCCESS);
        } else if (r.status == 403 && r.json().response == 'PHONE_VERIFICATION_CODE_INVALID') {
          simpleBorrowerRegistrationCallback(UserRegisterResponse.PHONE_VERIFICATION_CODE_MISMATCH);
        } else if (r.status == 409) {
          simpleBorrowerRegistrationCallback(UserRegisterResponse.EMAIL_ALREADY_EXISTS);
        } else {
          simpleBorrowerRegistrationCallback(UserRegisterResponse.UNKNOWN_ERROR);
        }
      }, (error) => {
        this._logger.logError('Error while registering new basic borrower: ' + JSON.stringify(error));
        simpleBorrowerRegistrationCallback(UserRegisterResponse.UNKNOWN_ERROR);
      });
  }

  public registerFullBorrower(user: User, auction: Auction, fullBorrowerRegistrationCallback: (response: UserRegisterResponse) => void): void {
    this._logger.logInfo('register new full borrower: ' + user + ' with loan application: ' + auction);
    this.http.post(RegistrationUrlConfiguration.FULL_BORROWER_REGISTRATION, JSON.stringify({
        email: user.email,
        phone: user.phone,
        phoneVerificationCode: user.phoneVerificationCode,
        password: user.password,
        name: user.name,
        surname: user.surname,
        registeredAddress: user.registeredAddress,
        correspondenceAddress: user.correspondenceAddress,
        ratingParameters: user.ratingParameters,
        loanApplication: auction
      }),
      {headers: this.headersForRequest})
      .subscribe((r: Response) => {
        if (r.status == 201) {
          fullBorrowerRegistrationCallback(UserRegisterResponse.SUCCESS);
        } else if (r.status == 403 && r.json().response == 'PHONE_VERIFICATION_CODE_INVALID') {
          fullBorrowerRegistrationCallback(UserRegisterResponse.PHONE_VERIFICATION_CODE_MISMATCH);
        } else if (r.status == 409) {
          fullBorrowerRegistrationCallback(UserRegisterResponse.EMAIL_ALREADY_EXISTS);
        } else {
          fullBorrowerRegistrationCallback(UserRegisterResponse.UNKNOWN_ERROR);
        }
      }, (error) => {
        this._logger.logError('Error while registering new full borrower: ' + JSON.stringify(error));
        fullBorrowerRegistrationCallback(UserRegisterResponse.UNKNOWN_ERROR);
      });
  }

  public registerBasicInvestor(user: User, simpleBorrowerRegistrationCallback: (response: UserRegisterResponse) => void): void {
    console.log('register new basic investor: ' + user);
    this.http.post(RegistrationUrlConfiguration.BASIC_INVESTOR_REGISTRATION, JSON.stringify({
        email: user.email,
        phone: user.phone,
        phoneVerificationCode: user.phoneVerificationCode,
        password: user.password,
        name: user.name,
        surname: user.surname
      }),
      {headers: this.headersForRequest})
      .subscribe((r: Response) => {
        if (r.status == 201) {
          simpleBorrowerRegistrationCallback(UserRegisterResponse.SUCCESS);
        } else if (r.status == 403 && r.json().response == 'PHONE_VERIFICATION_CODE_INVALID') {
          simpleBorrowerRegistrationCallback(UserRegisterResponse.PHONE_VERIFICATION_CODE_MISMATCH);
        } else if (r.status == 409) {
          simpleBorrowerRegistrationCallback(UserRegisterResponse.EMAIL_ALREADY_EXISTS);
        } else {
          simpleBorrowerRegistrationCallback(UserRegisterResponse.UNKNOWN_ERROR);
        }
      }, (error) => {
        this._logger.logError('Error while registering new basic investor: ' + JSON.stringify(error));
        simpleBorrowerRegistrationCallback(UserRegisterResponse.UNKNOWN_ERROR);
      });
  }

  public registerFullInvestor(user: User, fullInvestorRegistrationCallback: (response: UserRegisterResponse) => void): void {
    console.log('register new full investor: ' + user);
    this.http.post(RegistrationUrlConfiguration.FULL_INVESTOR_REGISTRATION, JSON.stringify({
        phoneVerificationCode: user.phoneVerificationCode,
        representingPerson: {
          email: user.email,
          phone: user.phone,
          password: user.password,
          name: user.name,
          surname: user.surname,
        },
        company: {
          name: user.company.name,
          form: user.company.form,
          taxId: user.company.taxId,
          registeredAddress: user.company.registeredAddress,
          correspondenceAddress: user.company.correspondenceAddress
        }
      }),
      {headers: this.headersForRequest})
      .subscribe((r: Response) => {
        if (r.status == 201) {
          fullInvestorRegistrationCallback(UserRegisterResponse.SUCCESS);
        } else if (r.status == 403 && r.json().response == 'PHONE_VERIFICATION_CODE_INVALID') {
          fullInvestorRegistrationCallback(UserRegisterResponse.PHONE_VERIFICATION_CODE_MISMATCH);
        } else if (r.status == 409) {
          fullInvestorRegistrationCallback(UserRegisterResponse.EMAIL_ALREADY_EXISTS);
        } else {
          fullInvestorRegistrationCallback(UserRegisterResponse.UNKNOWN_ERROR);
        }
      }, (error) => {
        this._logger.logError('Error while registering new basic investor: ' + JSON.stringify(error));
        fullInvestorRegistrationCallback(UserRegisterResponse.UNKNOWN_ERROR);
      });
  }

}
