"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
var http_1 = require("@angular/http");
var Observable_1 = require("rxjs/Observable");
require('rxjs/add/operator/map');
require('rxjs/add/observable/throw');
require('rxjs/add/operator/catch');
var LegalRegulation_1 = require(".././legal.regulation");
var LegalRegulationsService = (function () {
    function LegalRegulationsService(http) {
        this.http = http;
        this.legalRegulationsBaseUrl = 'http://52.29.74.108:9080/legalRegulations';
        console.log('LegalRegulationsService created');
    }
    LegalRegulationsService.prototype.getRegulationsTitlesWithIds = function (user) {
        console.log('getRegulationsTitlesWithIds');
        var headersForRequest = new http_1.Headers({
            'Content-Type': 'application/json',
            'x-auth': user.token
        });
        return this.http.get(this.legalRegulationsBaseUrl + "/titles", { headers: headersForRequest })
            .map(function (r) { return LegalRegulationsService.mapLegalRegulationsResponse(r); })
            .catch(this.handleError);
    };
    LegalRegulationsService.prototype.getRegulationText = function (id, user) {
        console.log('getRegulationText by id: ' + id);
        var headersForRequest = new http_1.Headers({
            'Content-Type': 'application/json',
            'x-auth': user.token
        });
        return this.http.get(this.legalRegulationsBaseUrl + "/" + id, { headers: headersForRequest })
            .map(function (r) { return LegalRegulationsService.mapLegalRegulationResponse(r); })
            .catch(this.handleError);
    };
    LegalRegulationsService.prototype.saveRegulation = function (legalRegulation, user) {
        console.log('saveRegulation: ' + JSON.stringify(legalRegulation));
        var headersForRequest = new http_1.Headers({
            'Content-Type': 'application/json',
            'x-auth': user.token
        });
        return this.http.post(this.legalRegulationsBaseUrl, JSON.stringify(legalRegulation), { headers: headersForRequest })
            .map(function (r) { return r.status == 201; })
            .catch(this.handleError);
    };
    LegalRegulationsService.prototype.updateRegulation = function (legalRegulation, user) {
        console.log('updateRegulation: ' + JSON.stringify(legalRegulation));
        var headersForRequest = new http_1.Headers({
            'Content-Type': 'application/json',
            'x-auth': user.token
        });
        return this.http.put(this.legalRegulationsBaseUrl, JSON.stringify(legalRegulation), { headers: headersForRequest })
            .map(function (r) { return r.status == 200; })
            .catch(this.handleError);
    };
    LegalRegulationsService.mapLegalRegulationsResponse = function (r) {
        if (r.status == 500) {
            console.log("Unknown error while getting legal moderation");
            return new LegalRegulation_1.LegalRegulation[0];
        }
        else if (r.status == 403) {
            console.log("Authentication error");
            return new LegalRegulation_1.LegalRegulation[0];
        }
        else if (r.status == 404) {
            console.log("No legal moderation found");
            return new LegalRegulation_1.LegalRegulation[0];
        }
        else {
            console.log(JSON.stringify(r.json()));
            return r.json();
        }
    };
    LegalRegulationsService.mapLegalRegulationResponse = function (r) {
        if (r.status == 500) {
            console.log("Unknown error while getting legal regulation");
            return null;
        }
        else if (r.status == 403) {
            console.log("Authentication error");
            return null;
        }
        else if (r.status == 404) {
            console.log("No legal regulation found");
            return null;
        }
        else {
            return r.json();
        }
    };
    LegalRegulationsService.prototype.handleError = function (error) {
        return Observable_1.Observable.throw(error);
    };
    LegalRegulationsService = __decorate([
        core_1.Injectable(),
        __metadata('design:paramtypes', [http_1.Http])
    ], LegalRegulationsService);
    return LegalRegulationsService;
}());
exports.LegalRegulationsService = LegalRegulationsService;
//# sourceMappingURL=legal.moderation.service.js.map
