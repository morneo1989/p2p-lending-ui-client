import {Injectable} from "@angular/core"
import {User} from "../../model/user/User";
import {Headers, Http, Response} from "@angular/http"

import {Observable} from "rxjs/Observable";

import 'rxjs/add/operator/map';
import 'rxjs/add/observable/throw';
import 'rxjs/add/operator/catch';
import {LegalRegulation} from "../../model/moderation/legal.regulation";
import {AppLoggerFactory} from "../../utilities/app.logger.factory";
import {LegalRegulationsMapper} from "../../model/mapping/legal.regulations.mapper";

@Injectable()
export class LegalRegulationsService {

  private _logger = this._loggerFactory.getLogger('UsersRegistrationService');

  private headersForRequest = new Headers({
    'Content-Type': 'application/json',
    'x-auth': ''
  });
  private legalRegulationsBaseUrl = 'http://52.29.74.108:9080/moderation/legalRegulations';

  constructor(private http: Http, private _loggerFactory: AppLoggerFactory, private mapper: LegalRegulationsMapper) {
    this._logger.logInfo('service created')
  }

  public getRegulationsTitlesWithIds(regulationsTitlesCallback: (titles: LegalRegulation[]) => void): void {
    this.http.get(this.legalRegulationsBaseUrl + "/titles", {headers: this.headersForRequest})
      .subscribe((r: Response) => {
          regulationsTitlesCallback(r.json().map((regulation) => this.mapper.mapFromRestModel(regulation)));
        },
        (error) => {
          this._logger.logError('Error: ' + JSON.stringify(error));
          regulationsTitlesCallback([]);
        });
  }

  public getRegulationText(id: String, regulationTextCallback: (legalRegulation: LegalRegulation) => void): void {
    this.http.get(this.legalRegulationsBaseUrl + "/" + id, {headers: this.headersForRequest})
      .subscribe((r: Response) => {
          regulationTextCallback(this.mapper.mapFromRestModel(r.json()));
        },
        (error) => {
          this._logger.logError('Error: ' + JSON.stringify(error));
          regulationTextCallback(null);
        });
  }

  public saveRegulation(legalRegulation: LegalRegulation, user: User, saveCallback: (isSaved: boolean) => void): void {
    this.headersForRequest.set('x-auth', user.token);
    this.http.post(this.legalRegulationsBaseUrl, JSON.stringify(this.mapper.mapToRestModel(legalRegulation)), {headers: this.headersForRequest})
      .subscribe((r: Response) => {
          saveCallback(true);
        },
        (error) => {
          this._logger.logError('Error: ' + JSON.stringify(error));
          saveCallback(false);
        });
  }

  public updateRegulation(legalRegulation: LegalRegulation, user: User, updateCallback: (isUpdated: boolean) => void): void {
    this.headersForRequest.set('x-auth', user.token);
    this.http.post(this.legalRegulationsBaseUrl, JSON.stringify(this.mapper.mapToRestModel(legalRegulation)), {headers: this.headersForRequest})
      .subscribe((r: Response) => {
          updateCallback(true);
        },
        (error) => {
          this._logger.logError('Error: ' + JSON.stringify(error));
          updateCallback(false);
        });
  }

}
