import {Injectable} from "@angular/core";
import {User} from "../../model/user/User";
import {Headers, Http, Response} from "@angular/http";
import "rxjs/add/operator/map";
import "rxjs/add/observable/throw";
import "rxjs/add/operator/catch";
import {AppLoggerFactory} from "../../utilities/app.logger.factory";
import {AboutUs} from "../../model/moderation/about.us";
import {P2PAbout} from "../../model/moderation/p2p.about";
import {InvestGuide} from "../../model/moderation/invest.guide";
import {BorrowGuide} from "../../model/moderation/borrow.guide";
import {Contact} from "../../model/moderation/contact";
import {Faq} from "../../model/moderation/faq";

@Injectable()
export class ContentModerationService {

  private _logger = this._loggerFactory.getLogger('ContentModerationService');

  private headersForRequest = new Headers({
    'Content-Type': 'application/json',
    'x-auth': ''
  });
  private aboutUsBaseUrl = 'http://52.29.74.108:9080/moderation/aboutUs';
  private faqBaseUrl = 'http://52.29.74.108:9080/moderation/faq';
  private p2pAboutBaseUrl = 'http://52.29.74.108:9080/moderation/p2pAbout';
  private investGuideBaseUrl = 'http://52.29.74.108:9080/moderation/investGuide';
  private borrowGuideBaseUrl = 'http://52.29.74.108:9080/moderation/borrowGuide';
  private contactBaseUrl = 'http://52.29.74.108:9080/moderation/contact';

  constructor(private http: Http, private _loggerFactory: AppLoggerFactory) {
    this._logger.logInfo('service created')
  }

  public getAboutUsContent(aboutUsContentCallback: (content: AboutUs) => void): void {
    this.http.get(this.aboutUsBaseUrl, {headers: this.headersForRequest})
      .subscribe((r: Response) => {
          aboutUsContentCallback(r.json() as AboutUs);
        },
        (error) => {
          this._logger.logError('Error: ' + JSON.stringify(error));
          aboutUsContentCallback(null);
        });
  }

  public saveOrUpdateAboutUs(aboutUs: AboutUs, user: User, saveCallback: (isSaved: boolean) => void): void {
    this.headersForRequest.set('x-auth', user.token);
    this.http.post(this.aboutUsBaseUrl, JSON.stringify(aboutUs), {headers: this.headersForRequest})
      .subscribe((r: Response) => {
          saveCallback(true);
        },
        (error) => {
          this._logger.logError('Error: ' + JSON.stringify(error));
          saveCallback(false);
        });
  }

  public getP2PAboutContent(p2pAboutContentCallback: (content: P2PAbout) => void): void {
    this.http.get(this.p2pAboutBaseUrl, {headers: this.headersForRequest})
      .subscribe((r: Response) => {
          p2pAboutContentCallback(r.json() as P2PAbout);
        },
        (error) => {
          this._logger.logError('Error: ' + JSON.stringify(error));
          p2pAboutContentCallback(null);
        });
  }

  public saveOrUpdateP2PAbout(p2pAbout: P2PAbout, user: User, callback: (isSaved: boolean) => void): void {
    this.headersForRequest.set('x-auth', user.token);
    this.http.post(this.p2pAboutBaseUrl, JSON.stringify(p2pAbout), {headers: this.headersForRequest})
      .subscribe((r: Response) => {
          callback(true);
        },
        (error) => {
          this._logger.logError('Error: ' + JSON.stringify(error));
          callback(false);
        });
  }

  public getInvestGuideContent(investGuideContentCallback: (content: InvestGuide) => void): void {
    this.http.get(this.investGuideBaseUrl, {headers: this.headersForRequest})
      .subscribe((r: Response) => {
          investGuideContentCallback(r.json() as InvestGuide);
        },
        (error) => {
          this._logger.logError('Error: ' + JSON.stringify(error));
          investGuideContentCallback(null);
        });
  }

  public saveOrUpdateInvestGuide(investGuide: InvestGuide, user: User, callback: (isSaved: boolean) => void): void {
    this.headersForRequest.set('x-auth', user.token);
    this.http.post(this.investGuideBaseUrl, JSON.stringify(investGuide), {headers: this.headersForRequest})
      .subscribe((r: Response) => {
          callback(true);
        },
        (error) => {
          this._logger.logError('Error: ' + JSON.stringify(error));
          callback(false);
        });
  }

  public getBorrowGuideContent(borrowGuideContentCallback: (content: BorrowGuide) => void): void {
    this.http.get(this.borrowGuideBaseUrl, {headers: this.headersForRequest})
      .subscribe((r: Response) => {
          borrowGuideContentCallback(r.json() as BorrowGuide);
        },
        (error) => {
          this._logger.logError('Error: ' + JSON.stringify(error));
          borrowGuideContentCallback(null);
        });
  }

  public saveOrUpdateBorrowGuide(borrowGuide: BorrowGuide, user: User, callback: (isSaved: boolean) => void): void {
    this.headersForRequest.set('x-auth', user.token);
    this.http.post(this.borrowGuideBaseUrl, JSON.stringify(borrowGuide), {headers: this.headersForRequest})
      .subscribe((r: Response) => {
          callback(true);
        },
        (error) => {
          this._logger.logError('Error: ' + JSON.stringify(error));
          callback(false);
        });
  }

  public getContactContent(contactContentCallback: (content: Contact) => void): void {
    this.http.get(this.contactBaseUrl, {headers: this.headersForRequest})
      .subscribe((r: Response) => {
          contactContentCallback(r.json() as Contact);
        },
        (error) => {
          this._logger.logError('Error: ' + JSON.stringify(error));
          contactContentCallback(null);
        });
  }

  public saveOrUpdateContact(contact: Contact, user: User, callback: (isSaved: boolean) => void): void {
    this.headersForRequest.set('x-auth', user.token);
    this.http.post(this.contactBaseUrl, JSON.stringify(contact), {headers: this.headersForRequest})
      .subscribe((r: Response) => {
          callback(true);
        },
        (error) => {
          this._logger.logError('Error: ' + JSON.stringify(error));
          callback(false);
        });
  }

  public getFaqContent(faqContentCallback: (content: Faq[]) => void): void {
    this.http.get(this.faqBaseUrl, {headers: this.headersForRequest})
      .subscribe((r: Response) => {
          faqContentCallback(r.json() as Faq[]);
        },
        (error) => {
          this._logger.logError('Error: ' + JSON.stringify(error));
          faqContentCallback(null);
        });
  }

  public saveOrUpdateFaq(faq: Faq[], user: User, callback: (isSaved: boolean) => void): void {
    this.headersForRequest.set('x-auth', user.token);
    this.http.post(this.faqBaseUrl, JSON.stringify(faq), {headers: this.headersForRequest})
      .subscribe((r: Response) => {
          callback(true);
        },
        (error) => {
          this._logger.logError('Error: ' + JSON.stringify(error));
          callback(false);
        });
  }

}
