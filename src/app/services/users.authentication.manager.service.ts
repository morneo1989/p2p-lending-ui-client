import {Injectable} from "@angular/core"
import {Router} from '@angular/router';
import {User} from "../model/user/User";

import {SessionStorageService} from "./session.storage.service";
import {UsersLoginService} from "./users.login.service";
import {AppLoggerFactory} from "../utilities/app.logger.factory";
import {isNullOrUndefined} from "util";

@Injectable()
export class UsersAuthenticationManagerService {

  private _logger = this._loggerFactory.getLogger('UsersAuthenticationManagerService');

  public constructor(private sessionStorage: SessionStorageService, private router: Router, private loginService: UsersLoginService,
                     private _loggerFactory: AppLoggerFactory) {
    this._logger.logInfo('service created');
  }

  public getCurrentUserFromSession(callback: (retrievedUser: User) => void, callbackLoginTimeout: () => void): void {
    this.sessionStorage.getAuthenticatedUser(
      (retrievedUser) => {
        if (isNullOrUndefined(retrievedUser.token)) {
          this._logger.logInfo('Clearing user from session');
          this.sessionStorage.clearAuthenticatedUser((isError: boolean) => {
            if (isError) {
              this._logger.logInfo("Error while clearing user occurred");
            } else {
              this.loginService.setLoggedInUser(new User());
              callback(new User());
            }
          });
        } else {
          this._logger.logInfo('validate token');
          this.loginService.validateUserLogin(retrievedUser, (validationResponse: boolean) => {
            if (validationResponse) {
              this._logger.logInfo('User session valid');
              callback(retrievedUser);
            } else {
              this._logger.logInfo('User login timeout occurred');
              this.sessionStorage.clearAuthenticatedUser((isError: boolean) => {
                if (isError) {
                  this._logger.logInfo("Error while clearing user occurred");
                } else {
                  this.loginService.setLoggedInUser(new User());
                  callbackLoginTimeout();
                  this.router.navigate(['/pospo/logout-timeout']);
                }
              });
              this.loginService.setLoggedInUser(new User());
              this.router.navigate(['/pospo/logout-timeout']);
            }
          });
        }
      },
      () => {
        //no user authentication data in session stored
        this.loginService.setLoggedInUser(new User());
        callback(new User());
      }
    );
  }

}
