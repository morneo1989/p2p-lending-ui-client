import {Injectable} from "@angular/core"
import {Headers, Http, Response} from "@angular/http"

import {Observable} from "rxjs/Observable";
import {Subject} from "rxjs/Subject";
import {UserLoginResponse} from "../model/user/UserLoginResponse";
import {User} from "../model/user/User";
import {AppLoggerFactory} from "../utilities/app.logger.factory";

@Injectable()
export class UsersLoginService {

  private _logger = this._loggerFactory.getLogger('UsersLoginService');

  private headersForRequest = new Headers({
    'Content-Type': 'application/json',
    'Access-Control-Expose-Headers': 'x-auth'
  });
  private usersBaseUrl = 'http://52.29.74.108:9080/users/login';

  private loggedInUser: User;

  public loginUserChanged: Subject<boolean>;

  constructor(private http: Http, private _loggerFactory: AppLoggerFactory) {
    this.loginUserChanged = new Subject<boolean>();
    this._logger.logInfo('service created');
  }

  public getLoggedInUser() {
    return this.loggedInUser;
  }

  public setLoggedInUser(user: User) {
    this.loggedInUser = user;
    this.loginUserChanged.next(true);
  }

  public loginUser(user: User, loginCallback: (response: UserLoginResponse) => void): void {
    this.http.post(this.usersBaseUrl, JSON.stringify({
      email: user.email,
      password: user.password
    }), {headers: this.headersForRequest})
      .subscribe((r: Response) => {
          let userLoginResponse = new UserLoginResponse();
          userLoginResponse.name = r.json().name;
          userLoginResponse.surname = r.json().surname;
          userLoginResponse.userType = r.json().userType;
          userLoginResponse.emailVerified = r.json().emailVerified;
          userLoginResponse.token = r.headers.get('x-auth');
          this._logger.logDebug('response headers: ' + JSON.stringify(r.headers.toJSON()));
          this._logger.logDebug('response token header: ' + r.headers.get('x-auth'));
          loginCallback(userLoginResponse);
        },
        (error) => {
          this._logger.logError('Error while login: ' + JSON.stringify(error));
          let userLoginResponse = new UserLoginResponse();
          userLoginResponse.passwordInvalid = error.status == 403;
          userLoginResponse.userUnknown = error.status == 404;
          userLoginResponse.unknownError = error.status == 500 || error.status == 400;
          loginCallback(userLoginResponse);
        });
  }

  public validateUserLogin(user: User, validateCallback: (isValid: boolean) => void): void {
    this.http.get(this.usersBaseUrl + '/validate', {
      headers: new Headers({
        'Content-Type': 'application/json',
        'x-auth': user.token
      })
    }).subscribe((r: Response) => {
        validateCallback(true);
      },
      (error) => {
        this._logger.logError('Error while login: ' + JSON.stringify(error));
        validateCallback(false);
      })
  }

}
