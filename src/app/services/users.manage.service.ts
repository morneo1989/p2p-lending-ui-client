import {Injectable} from "@angular/core"
import {AppLoggerFactory} from "../utilities/app.logger.factory";

@Injectable()
export class UsersManageService {

  private _logger = this._loggerFactory.getLogger('UsersManageService');

  public constructor(private _loggerFactory: AppLoggerFactory) {
    this._logger.logInfo('service created')
  }
}
