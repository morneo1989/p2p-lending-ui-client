import {Injectable} from "@angular/core";
import {User} from "../model/user/User";
import {AsyncLocalStorage} from 'angular2-async-local-storage';
import {AppLoggerFactory} from "../utilities/app.logger.factory";

@Injectable()
export class SessionStorageService {

  private _logger = this._loggerFactory.getLogger('SessionStorageService');

  public constructor(private sessionStorage: AsyncLocalStorage, private _loggerFactory: AppLoggerFactory) {
    console.log('SessionStorageService created');
  }

  public getAuthenticatedUser(callbackSuccessFunction: (retrievedUser: User) => void, callbackErrorFunction: () => void) {
    this.sessionStorage.getItem('authUser').subscribe(
      (retrievedUser: User) => {
        if (retrievedUser != null) {
          this._logger.logInfo('User data retrieved from session: ' + JSON.stringify(retrievedUser));
          callbackSuccessFunction(retrievedUser);
        } else {
          this._logger.logInfo('No user authentication data in session stored');
          callbackErrorFunction();
        }

      },
      () => {
        this._logger.logError('No user authentication data in session stored');
      });
  }

  public saveAuthenticatedUser(user: User, callbackFunction: (isError: boolean) => void) {
    this.sessionStorage.setItem('authUser', user).subscribe(
      () => {
        this._logger.logInfo('User data stored in session');
        callbackFunction(false);
      },
      () => {
        this._logger.logWarn('User data not saved in session');
        callbackFunction(true);
      });
  }

  public clearAuthenticatedUser(callbackFunction: (isError: boolean) => void) {
    this.saveAuthenticatedUser(new User(), callbackFunction)
  }

}
