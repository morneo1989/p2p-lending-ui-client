"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
var http_1 = require("@angular/http");
var Observable_1 = require("rxjs/Observable");
var auction_1 = require("../model/auction/auction");
require('rxjs/add/operator/map');
require('rxjs/add/observable/throw');
require('rxjs/add/operator/catch');
var AuctionsService = (function () {
    function AuctionsService(http) {
        this.http = http;
        this.usersBaseUrl = 'http://52.29.74.108:9080/auctions';
        console.log('AuctionsService created');
    }
    AuctionsService.prototype.createAuction = function (user, auction) {
        console.log('createAuction: ' + auction);
        var headersForRequest = new http_1.Headers({
            'Content-Type': 'application/json',
            'x-auth': user.token
        });
        return this.http.post(this.usersBaseUrl + "/application", JSON.stringify(auction), { headers: headersForRequest })
            .map(function (r) { return r.status == 201; })
            .catch(this.handleError);
    };
    AuctionsService.prototype.getAuctions = function (user) {
        console.log('getAuctions: ');
        var headersForRequest = new http_1.Headers({
            'Content-Type': 'application/json',
            'x-auth': user.token
        });
        return this.http.get(this.usersBaseUrl, { headers: headersForRequest })
            .map(function (r) { return AuctionsService.mapAuctionsResponse(r); })
            .catch(this.handleError);
    };
    AuctionsService.mapAuctionsResponse = function (r) {
        if (r.status == 500) {
            console.log("Unknown error while getting auctions");
            return new auction_1.Auction[0];
        }
        else if (r.status == 403) {
            console.log("Authentication error");
            return new auction_1.Auction[0];
        }
        else if (r.status == 404) {
            console.log("No auctions found");
            return new auction_1.Auction[0];
        }
        else {
            return r.json();
        }
    };
    AuctionsService.prototype.handleError = function (error) {
        return Observable_1.Observable.throw(error);
    };
    AuctionsService = __decorate([
        core_1.Injectable(), 
        __metadata('design:paramtypes', [http_1.Http])
    ], AuctionsService);
    return AuctionsService;
}());
exports.AuctionsService = AuctionsService;
//# sourceMappingURL=auctions.service.js.map