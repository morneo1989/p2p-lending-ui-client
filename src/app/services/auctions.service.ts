import {Injectable} from "@angular/core"
import {User} from "../model/user/User";
import {Headers, Http, Response} from "@angular/http"

import {Observable} from "rxjs/Observable";
import {Auction} from "../model/auction/auction";

import 'rxjs/add/operator/map';
import 'rxjs/add/observable/throw';
import 'rxjs/add/operator/catch';
import {AppLoggerFactory} from "../utilities/app.logger.factory";
import {UserType} from "../model/user/enums/user.type";

@Injectable()
export class AuctionsService {
  private _logger = this._loggerFactory.getLogger('AuctionsService');

  private headersForRequest = new Headers({
    'Content-Type': 'application/json',
    'x-auth': ''
  });
  private auctionsBaseUrl = 'http://52.29.74.108:9080/auctions';

  constructor(private http: Http, private _loggerFactory: AppLoggerFactory) {
    this._logger.logInfo('service created')
  }

  public saveAuction(user: User, auction: Auction, saveCallback: (isSaved: boolean) => void): void {
    this.headersForRequest.set('x-auth', user.token);
    this.http.post(this.auctionsBaseUrl + "/application", JSON.stringify(auction), {headers: this.headersForRequest})
      .subscribe((r: Response) => {
          saveCallback(true);
        },
        (error) => {
          this._logger.logError('Error while saving auction: ' + JSON.stringify(error));
          saveCallback(false);
        })
  }

  public getAllAuctions(user: User, auctionsCallback: (auctions: Auction[]) => void): void {
    this.headersForRequest.set('x-auth', user.token);
    this.http.get(this.auctionsBaseUrl, {headers: this.headersForRequest})
      .subscribe((r: Response) => {
          auctionsCallback(r.json() as Auction[]);
        },
        (error) => {
          this._logger.logError('Error while getting auctions: ' + JSON.stringify(error));
          auctionsCallback([]);
        })
  }

  public getUserAuctions(user: User, auctionsCallback: (auctions: Auction[]) => void): void {
    this.headersForRequest.set('x-auth', user.token);
    let url = this.auctionsBaseUrl;
    if(user.userType == UserType.ADMIN) {
      url = this.auctionsBaseUrl + '/admin'
    } else if(user.userType == UserType.INVESTOR) {
      url = this.auctionsBaseUrl + '/investor'
    } else if(user.userType == UserType.BORROWER) {
      url = this.auctionsBaseUrl + '/borrower'
    }
    this.http.get(url, {headers: this.headersForRequest})
      .subscribe((r: Response) => {
          auctionsCallback(r.json() as Auction[]);
        },
        (error) => {
          this._logger.logError('Error while getting auctions: ' + JSON.stringify(error));
          auctionsCallback([]);
        })
  }

}
