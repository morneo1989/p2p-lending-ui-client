import {Injectable} from "@angular/core"
import {AppLoggerFactory} from "../utilities/app.logger.factory";
import {User} from "../model/user/User";
import {Subject} from "rxjs";

@Injectable()
export class ApplicationDataSynchronizationService {

  private _logger = this._loggerFactory.getLogger('ApplicationDataSynchronizationService');

  public loggedInUser: User;
  public loggedInUserSubject: Subject<User>;

  public constructor(private _loggerFactory: AppLoggerFactory) {
    this.loggedInUser = new User();
    this.loggedInUserSubject = new Subject<User>();
    this._logger.logInfo('service created')
  }

  public setLoggedInUser(user: User) {
    this.loggedInUser = user;
    this.loggedInUserSubject.next(user);
  }

}
