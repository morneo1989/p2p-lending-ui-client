import {Injectable} from "@angular/core"
import {User} from "../model/user/User";
import {Headers, Http, Response} from "@angular/http"

import {Observable} from "rxjs/Observable";
import {Auction} from "../model/auction/auction";

import 'rxjs/add/operator/map';
import 'rxjs/add/observable/throw';
import 'rxjs/add/operator/catch';
import {AppLoggerFactory} from "../utilities/app.logger.factory";
import {UserType} from "../model/user/enums/user.type";
import {InvestorShare} from "../model/investment/investorShare";

@Injectable()
export class InvestmentsService {
  private _logger = this._loggerFactory.getLogger('InvestmentsService');

  private headersForRequest = new Headers({
    'Content-Type': 'application/json',
    'x-auth': ''
  });
  private investmentsBaseUrl = 'http://52.29.74.108:9080/investments';

  constructor(private http: Http, private _loggerFactory: AppLoggerFactory) {
    this._logger.logInfo('service created')
  }

  public saveInvestment(user: User, investorShare: InvestorShare, saveCallback: (isSaved: boolean) => void): void {
    this.headersForRequest.set('x-auth', user.token);
    this.http.post(this.investmentsBaseUrl, JSON.stringify(investorShare), {headers: this.headersForRequest})
      .subscribe((r: Response) => {
          saveCallback(true);
        },
        (error) => {
          this._logger.logError('Error while saving investment: ' + JSON.stringify(error));
          saveCallback(false);
        })
  }

  public getInvestments(user: User, investmentsCallback: (investorShares: InvestorShare[]) => void): void {
    this.headersForRequest.set('x-auth', user.token);
    this.http.get(this.investmentsBaseUrl, {headers: this.headersForRequest})
      .subscribe((r: Response) => {
          investmentsCallback(r.json() as InvestorShare[]);
        },
        (error) => {
          this._logger.logError('Error while getting investments: ' + JSON.stringify(error));
          investmentsCallback([]);
        })
  }

}
