import {RestUrlConfiguration} from "./rest.url.configuration";
export class RegistrationUrlConfiguration {
  private static BASE_URL = RestUrlConfiguration.BASE_URL + "users/registration/";
  public static BASIC_BORROWER_REGISTRATION = RegistrationUrlConfiguration.BASE_URL + "/basic/borrower";
  public static FULL_BORROWER_REGISTRATION = RegistrationUrlConfiguration.BASE_URL + "/full/borrower";
  public static BASIC_INVESTOR_REGISTRATION = RegistrationUrlConfiguration.BASE_URL + "/basic/investor";
  public static FULL_INVESTOR_REGISTRATION = RegistrationUrlConfiguration.BASE_URL + "/full/investor";
  public static PHONE_VERIFICATION = RegistrationUrlConfiguration.BASE_URL + "/phone/verification";
}
