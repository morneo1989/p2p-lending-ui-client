import {Injectable} from "@angular/core";
import {Http} from "@angular/http";
import {AppLoggerFactory} from "../utilities/app.logger.factory";
@Injectable()
export class FormsDataInputStorage {

  private _logger = this._loggerFactory.getLogger('FormsDataInputStorage');

  private configuration = {
    LOAN_TARGETS: [],
    EMPLOYMENT_AREAS: [],
    INCOME_SOURCES: [],
    MARITAL_STATUSES: [],
    COMPANY_FORMS: []
  };

  private configurationLoadCallback = [];

  constructor(private http: Http, private _loggerFactory: AppLoggerFactory) {
    this.http.get('resources/config/forms.data.input.json')
      .map(res => res.json())
      .subscribe((config) => {
        this.configuration = config;
        if(this.configurationLoadCallback != null) {
          this.configurationLoadCallback.forEach((callback) => {
            callback(this.configuration);
          })
        }
      });
    this._logger.logInfo('service created');
  }

  public subscribeForConfigurationLoadEvent(callback: (configuration) => void) {
    this.configurationLoadCallback.push(callback);
    callback(this.configuration);
  }


}
