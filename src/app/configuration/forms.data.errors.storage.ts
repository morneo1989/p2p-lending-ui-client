import {Injectable} from "@angular/core";
import {Http} from "@angular/http";
import {AppLoggerFactory} from "../utilities/app.logger.factory";
@Injectable()
export class FormsDataErrorStorage {

  private _logger = this._loggerFactory.getLogger('FormsDataErrorStorage');

  private configuration = {
    REGISTRATION: [],
    LOANS: [],
    LOGIN: [],
    LEGAL_REGULATIONS: []
  };

  private configurationLoadCallback = [];

  constructor(private http: Http, private _loggerFactory: AppLoggerFactory) {
    this.http.get('resources/config/forms.data.errors.json')
      .map(res => res.json())
      .subscribe((config) => {
        this.configuration = config;
        if(this.configurationLoadCallback != null) {
          this.configurationLoadCallback.forEach((callback) => {
            callback(this.configuration);
          })
        }
      })
    this._logger.logInfo('service created');
  }

  public subscribeForConfigurationLoadEvent(callback: (configuration) => void) {
    this.configurationLoadCallback.push(callback);
    callback(this.configuration);
  }

}
