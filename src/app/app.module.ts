import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {HttpModule} from '@angular/http';
import {RouterModule} from "@angular/router";
import {WelcomeComponent} from "./components/welcome";
import {LegalRegulationsModerationComponent} from "./components/moderation/legal.regulations.moderation";
import {WelcomeNewUserComponent} from "./components/welcome-new-user";
import {UserLoggedOutComponent} from "./components/user.logged.out";
import {UserLoginTimeoutComponent} from "./components/user.login.timeout";
import {RegistrationComponent} from "./components/registration";
import {AdminPanelComponent} from "./components/admin-panel";
import {InvestorPanelComponent} from "./components/investor.panel";
import {BorrowerPanelComponent} from "./components/borrower.panel";
import {AppMainPageComponent} from "./components/app.main";
import {FooterComponent} from "./components/footer";
import {PartnersComponent} from "./components/partners";
import {PromotedAuctionsComponent} from "./components/promoted.auctions";
import {ReferencesComponent} from "./components/references";
import {StatisticsComponent} from "./components/statistics";
import {TopMenuComponent} from "./components/top.menu";
import {SessionStorageService} from "./services/session.storage.service";
import {LegalRegulationsService} from "./services/moderation/legal.regulations.service";
import {UsersAuthenticationManagerService} from "./services/users.authentication.manager.service";
import {UsersLoginService} from "./services/users.login.service";
import {UsersManageService} from "./services/users.manage.service";
import {AuctionsService} from "./services/auctions.service";
import {UsersRegistrationService} from "./services/users.registration.service";
import {AsyncLocalStorageModule} from "angular2-async-local-storage";
import { QuillEditorModule } from 'ng2-quill-editor';
import {FormsDataInputStorage} from "./configuration/forms.data.input.storage";
import {FormsDataErrorStorage} from "./configuration/forms.data.errors.storage";
import {AppLoggerFactory} from "./utilities/app.logger.factory";
import {AboutUsModerationComponent} from "./components/moderation/about.us.moderation";
import {P2PAboutModerationComponent} from "./components/moderation/p2p.about.moderation";
import {ContactModerationComponent} from "./components/moderation/contact.moderation";
import {BorrowGuideModerationComponent} from "./components/moderation/borrow.guide.moderation";
import {InvestGuideModerationComponent} from "./components/moderation/invest.guide.moderation";
import {FaqModerationComponent} from "./components/moderation/faq.moderation";
import {AdminPanelPageController} from "./controllers/admin.panel.controller";
import {AuctionLoanApplicationPageController} from "./controllers/auction.loan.application.controller";
import {BorrowerPanelPageController} from "./controllers/borrower.panel.controller";
import {ContentModerationPageController} from "./controllers/content.moderation.controller";
import {InvestorPanelPageController} from "./controllers/investor.panel.controller";
import {PromotedAuctionsPageController} from "./controllers/promoted.auctions.controller";
import {RegistrationPanelPageController} from "./controllers/registration.controller";
import {StatisticsPageController} from "./controllers/statistics.controller";
import {TopMenuPanelPageController} from "./controllers/top.menu.controller";
import {Logger} from "angular2-logger/core";
import {ApplicationDataSynchronizationService} from "./services/application.data.synchronization.service";
import {LegalRegulationsMapper} from "./model/mapping/legal.regulations.mapper";
import {ContentModerationService} from "./services/moderation/content.moderation.service";
import {InvestmentsService} from "./services/investments.service";
import {FooterPageController} from "./controllers/footer.controller";
import {StaticContentPageController} from "./controllers/static.content.controller";
import {StaticContentComponent} from "./components/static.content";
import {FaqStaticContentComponent} from "./components/faq.static.content";
import {AuctionLoanApplicationPanelComponent} from "./components/auction.loan.application";
import {BorrowerAuctionsListPanelComponent} from "./components/borrower.auctions.list";
import {BorrowerAuctionsListPageController} from "./controllers/borrower.auctions.list.controller";

@NgModule({
  imports: [BrowserModule, HttpModule, AsyncLocalStorageModule, QuillEditorModule, ReactiveFormsModule, FormsModule,
    RouterModule.forRoot([
      {path: 'pospo', component: WelcomeComponent},
      {path: 'pospo/info', component: StaticContentComponent},
      {path: 'pospo/faq', component: FaqStaticContentComponent},
      {path: 'pospo/new-user', component: WelcomeNewUserComponent},
      {path: 'pospo/logout', component: UserLoggedOutComponent},
      {path: 'pospo/logout-timeout', component: UserLoginTimeoutComponent},
      {path: 'pospo/preregister', component: RegistrationComponent},
      {path: 'pospo/admin/panel', component: AdminPanelComponent},
      {path: 'pospo/investor/panel', component: InvestorPanelComponent},
      {path: 'pospo/borrower/panel', component: BorrowerPanelComponent},
      {path: 'pospo/statistics', component: StatisticsComponent},
      {path: 'pospo/promoted-auctions', component: PromotedAuctionsComponent}
    ])
  ],
  declarations: [AppMainPageComponent,
    AuctionLoanApplicationPanelComponent,
    BorrowerAuctionsListPanelComponent,
    StaticContentComponent,
    FaqStaticContentComponent,
    AboutUsModerationComponent,
    BorrowGuideModerationComponent,
    P2PAboutModerationComponent,
    ContactModerationComponent,
    InvestGuideModerationComponent,
    FaqModerationComponent,
    LegalRegulationsModerationComponent,
    UserLoggedOutComponent,
    UserLoginTimeoutComponent,
    WelcomeComponent,
    FooterComponent,
    PartnersComponent,
    PromotedAuctionsComponent,
    ReferencesComponent,
    StatisticsComponent,
    TopMenuComponent,
    WelcomeNewUserComponent,
    RegistrationComponent,
    AdminPanelComponent,
    InvestorPanelComponent,
    BorrowerPanelComponent],
  providers: [
    /* Services */
    SessionStorageService,
    LegalRegulationsService,
    ContentModerationService,
    UsersAuthenticationManagerService,
    UsersLoginService,
    UsersManageService,
    AuctionsService,
    InvestmentsService,
    UsersRegistrationService,
    /* Controllers */
    BorrowerAuctionsListPageController,
    StaticContentPageController,
    FooterPageController,
    AdminPanelPageController,
    AuctionLoanApplicationPageController,
    BorrowerPanelPageController,
    ContentModerationPageController,
    InvestorPanelPageController,
    PromotedAuctionsPageController,
    RegistrationPanelPageController,
    StatisticsPageController,
    TopMenuPanelPageController,
    /* Configuration Storage */
    FormsDataInputStorage,
    FormsDataErrorStorage,
    /* Mappers */
    LegalRegulationsMapper,
    /* Utilities */
    ApplicationDataSynchronizationService,
    Logger,
    AppLoggerFactory
  ],
  bootstrap: [AppMainPageComponent]
})
export class AppModule {
}
