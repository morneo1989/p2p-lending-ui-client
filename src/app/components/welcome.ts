import {Component} from "@angular/core";
import {Router} from "@angular/router";
import {AppLoggerFactory} from "../utilities/app.logger.factory";

@Component({
  selector: 'div[welcome]',
  templateUrl: '../templates/welcome.html'
})
export class WelcomeComponent {

  private _logger = this._loggerFactory.getLogger('WelcomeComponent');

  public constructor(private router: Router, private _loggerFactory: AppLoggerFactory) {
    this._logger.logInfo('component created');
  }
}
