import {Component} from "@angular/core";
import {AppLoggerFactory} from "../utilities/app.logger.factory";
import {Auction} from "../model/auction/auction";
import {LoadedAuctionsEvent} from "../model/events/loaded.auctions.event";
import {BorrowerAuctionsListPageController} from "../controllers/borrower.auctions.list.controller";

@Component({
  selector: 'div[borrower-auctions-list]',
  templateUrl: '../templates/borrower-auctions-list.html'
})
export class BorrowerAuctionsListPanelComponent {

  private _logger = this._loggerFactory.getLogger('BorrowerAuctionsListPanelComponent');

  public borrowerAuctions: Auction[];

  public constructor(private controller: BorrowerAuctionsListPageController, private _loggerFactory: AppLoggerFactory) {
    this.controller.subscribeForViewChangeEvents((viewChangeEvent) => {
      if(viewChangeEvent instanceof LoadedAuctionsEvent) {
        this.borrowerAuctions = (<LoadedAuctionsEvent>viewChangeEvent).auctions;
      }
    });
    this._logger.logInfo('component created')
  }


}
