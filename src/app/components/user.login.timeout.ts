import {Component} from "@angular/core";
import {AppLoggerFactory} from "../utilities/app.logger.factory";

@Component({
  selector: 'div[user-login-timeout]',
  templateUrl: '../templates/user-login-timeout.html'
})
export class UserLoginTimeoutComponent {

  private _logger = this._loggerFactory.getLogger('UserLoginTimeoutComponent');

  constructor(private _loggerFactory: AppLoggerFactory) {
    this._logger.logInfo('component created')
  }

}
