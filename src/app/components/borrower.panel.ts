import {Component, OnInit} from "@angular/core";
import {User} from "../model/user/User";
import {Router} from "@angular/router";
import {UsersLoginService} from "../services/users.login.service";
import {SessionStorageService} from "../services/session.storage.service";
import {UsersAuthenticationManagerService} from "../services/users.authentication.manager.service";
import {UserType} from "../model/user/enums/user.type";
import {AppLoggerFactory} from "../utilities/app.logger.factory";

@Component({
  selector: 'div[borrower-panel]',
  templateUrl: '../templates/borrower-panel.html'
})
export class BorrowerPanelComponent implements OnInit {

  private _logger = this._loggerFactory.getLogger('BorrowerPanelComponent');

  public borrower = new User();

  public isNewAuctionPanelVisible = false;
  public isAuctionsPanelVisible = false;

  public constructor(private router: Router, private sessionStorage: SessionStorageService,
                     private loginService: UsersLoginService, private usersAuthenticationManager: UsersAuthenticationManagerService,
                     private _loggerFactory: AppLoggerFactory) {
    loginService.loginUserChanged.subscribe((val) => this.borrower = this.loginService.getLoggedInUser());
    this._logger.logInfo('component created')
  }

  ngOnInit() {
    this.router.events.subscribe((val) => {
      if (val.url == '/pospo/borrower/panel') {
        this._logger.logDebug("On router navigation: " + val);
        this.usersAuthenticationManager.getCurrentUserFromSession(
          (retrievedUser: User) => {
            if (retrievedUser == null || retrievedUser.token == null || retrievedUser.userType != UserType.BORROWER) {
              if (this.loginService.getLoggedInUser().token == null || this.loginService.getLoggedInUser().userType != UserType.BORROWER) {
                this.router.navigate(['/pospo']);
                return;
              }
            }
          },
          () => {
            //do nothing if login timeout
          });
      }
    });
  }

  public logoutUser() {
    this.sessionStorage.clearAuthenticatedUser((isError: boolean) => {
      if (isError) {
        this._logger.logInfo("Error while clearing user occurred");
      } else {
        this.loginService.setLoggedInUser(new User());
        this.router.navigate(['/pospo/logout']);
      }
    });
  }

  public onSummaryClick() {
    this.isNewAuctionPanelVisible = false;
    this.isAuctionsPanelVisible = false;
  }

  public onNewAuctionClick() {
    this.isNewAuctionPanelVisible = true;
    this.isAuctionsPanelVisible = false;
  }

  public onAuctionsClick() {
    this.isNewAuctionPanelVisible = false;
    this.isAuctionsPanelVisible = true;
  }

}
