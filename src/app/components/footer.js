"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var Footer = (function () {
    function Footer(router) {
        this.router = router;
        this.legalRegulations = [{ id: 1, title: "Pierwszy regulamin do edycji" }, { id: 2, title: "Drugi regulamin do edycji" }];
        console.log('Footer created');
    }
    Footer.prototype.onLegalRegulationClicked = function (legalRegulationId) {
        this.router.navigate(['/pospo/legal_regulations', legalRegulationId]);
    };
    Footer = __decorate([
        core_1.Component({
            selector: 'pospo-footer',
            templateUrl: 'app/templates/pospo-footer.html'
        }), 
        __metadata('design:paramtypes', [router_1.Router])
    ], Footer);
    return Footer;
}());
exports.Footer = Footer;
//# sourceMappingURL=footer.js.map