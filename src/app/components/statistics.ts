import {Component} from "@angular/core"
import {AppLoggerFactory} from "../utilities/app.logger.factory";

@Component({
	selector: 'statistics',
	templateUrl: '../templates/statistics.html'
})

export class StatisticsComponent {

  private _logger = this._loggerFactory.getLogger('StatisticsComponent');

	public constructor(private _loggerFactory: AppLoggerFactory) {
    this._logger.logInfo('component created')
	}
}
