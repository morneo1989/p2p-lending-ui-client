import {Component} from "@angular/core";
import {User} from "../model/user/User";
import {Router} from "@angular/router";
import {UsersLoginService} from "../services/users.login.service";
import {SessionStorageService} from "../services/session.storage.service";
import {AppLoggerFactory} from "../utilities/app.logger.factory";

@Component({
  selector: 'div[admin-panel]',
  templateUrl: '../templates/admin-panel.html'
})
export class AdminPanelComponent {

  private _logger = this._loggerFactory.getLogger('AdminPanelComponent');

  public admin = new User();
  public isStatisticsPanelVisible = true;
  public isLegalRegulationsPanelVisible = false;
  public isAboutUsModeratePanelVisible = false;
  public isBorrowGuideModeratePanelVisible = false;
  public isInvestGuideModeratePanelVisible = false;
  public isContactModeratePanelVisible = false;
  public isP2PAboutModeratePanelVisible = false;
  public isFaqModeratePanelVisible = false;

  public constructor(private router: Router, private loginService: UsersLoginService,
                      private sessionStorage: SessionStorageService, private _loggerFactory: AppLoggerFactory) {
    loginService.loginUserChanged.subscribe((val: boolean) => this.admin = loginService.getLoggedInUser());
    this._logger.logInfo('AdminPanelComponent created');
  }

  public displayStatisticsPanel() {
    this.isFaqModeratePanelVisible = false;
    this.isP2PAboutModeratePanelVisible = false;
    this.isContactModeratePanelVisible = false;
    this.isInvestGuideModeratePanelVisible = false;
    this.isBorrowGuideModeratePanelVisible = false;
    this.isAboutUsModeratePanelVisible = false;
    this.isLegalRegulationsPanelVisible = false;
    this.isStatisticsPanelVisible = true;
  }

  public displayLegalRegulationsPanel() {
    this.isFaqModeratePanelVisible = false;
    this.isP2PAboutModeratePanelVisible = false;
    this.isContactModeratePanelVisible = false;
    this.isInvestGuideModeratePanelVisible = false;
    this.isBorrowGuideModeratePanelVisible = false;
    this.isAboutUsModeratePanelVisible = false;
    this.isLegalRegulationsPanelVisible = true;
    this.isStatisticsPanelVisible = false;
  }

  public displayAboutUsModeratePanel() {
    this.isFaqModeratePanelVisible = false;
    this.isP2PAboutModeratePanelVisible = false;
    this.isContactModeratePanelVisible = false;
    this.isInvestGuideModeratePanelVisible = false;
    this.isBorrowGuideModeratePanelVisible = false;
    this.isAboutUsModeratePanelVisible = true;
    this.isLegalRegulationsPanelVisible = false;
    this.isStatisticsPanelVisible = false;
  }

  public displayBorrowGuideModeratePanel() {
    this.isFaqModeratePanelVisible = false;
    this.isP2PAboutModeratePanelVisible = false;
    this.isContactModeratePanelVisible = false;
    this.isInvestGuideModeratePanelVisible = false;
    this.isBorrowGuideModeratePanelVisible = true;
    this.isAboutUsModeratePanelVisible = false;
    this.isLegalRegulationsPanelVisible = false;
    this.isStatisticsPanelVisible = false;
  }

  public displayInvestGuideModeratePanel() {
    this.isFaqModeratePanelVisible = false;
    this.isP2PAboutModeratePanelVisible = false;
    this.isContactModeratePanelVisible = false;
    this.isInvestGuideModeratePanelVisible = true;
    this.isBorrowGuideModeratePanelVisible = false;
    this.isAboutUsModeratePanelVisible = false;
    this.isLegalRegulationsPanelVisible = false;
    this.isStatisticsPanelVisible = false;
  }

  public displayContactModeratePanel() {
    this.isFaqModeratePanelVisible = false;
    this.isP2PAboutModeratePanelVisible = false;
    this.isContactModeratePanelVisible = true;
    this.isInvestGuideModeratePanelVisible = false;
    this.isBorrowGuideModeratePanelVisible = false;
    this.isAboutUsModeratePanelVisible = false;
    this.isLegalRegulationsPanelVisible = false;
    this.isStatisticsPanelVisible = false;
  }

  public displayP2PAboutModeratePanel() {
    this.isFaqModeratePanelVisible = false;
    this.isP2PAboutModeratePanelVisible = true;
    this.isContactModeratePanelVisible = false;
    this.isInvestGuideModeratePanelVisible = false;
    this.isBorrowGuideModeratePanelVisible = false;
    this.isAboutUsModeratePanelVisible = false;
    this.isLegalRegulationsPanelVisible = false;
    this.isStatisticsPanelVisible = false;
  }

  public displayFaqModeratePanel() {
    this.isFaqModeratePanelVisible = true;
    this.isP2PAboutModeratePanelVisible = false;
    this.isContactModeratePanelVisible = false;
    this.isInvestGuideModeratePanelVisible = false;
    this.isBorrowGuideModeratePanelVisible = false;
    this.isAboutUsModeratePanelVisible = false;
    this.isLegalRegulationsPanelVisible = false;
    this.isStatisticsPanelVisible = false;
  }

  public logoutUser() {
    this.sessionStorage.clearAuthenticatedUser((isError: boolean) => {
      if (isError) {
        this._logger.logInfo("Error while clearing user occurred");
      } else {
        this.loginService.setLoggedInUser(new User());
        this.router.navigate(['/pospo/logout']);
      }
    });
  }

}
