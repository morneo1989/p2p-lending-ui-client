import {Component} from "@angular/core";
import {Router, ActivatedRoute} from "@angular/router";
import {AppLoggerFactory} from "../utilities/app.logger.factory";
import {StaticContentPageController} from "../controllers/static.content.controller";
import {StaticContentType} from "../model/enum/static.content.type";
import {AboutUs} from "../model/moderation/about.us";
import {P2PAbout} from "../model/moderation/p2p.about";
import {BorrowGuide} from "../model/moderation/borrow.guide";
import {Contact} from "../model/moderation/contact";
import {InvestGuide} from "../model/moderation/invest.guide";
import {Faq} from "../model/moderation/faq";
import {LegalRegulation} from "../model/moderation/legal.regulation";
import {FaqContentEvent} from "../model/events/faq.content.event";

@Component({
  selector: 'div[faq-static-content]',
  templateUrl: '../templates/faq_static_content.html'
})
export class FaqStaticContentComponent {

  private _logger = this._loggerFactory.getLogger('FaqStaticContentComponent');

  private contentType: StaticContentType;

  public faqContentList: Faq[];

  public constructor(private route: ActivatedRoute, private router: Router, private _loggerFactory: AppLoggerFactory,
                     private controller: StaticContentPageController) {
    this.controller.subscribeForFaqContentViewChangeEvents((viewChangeEvent) => {
      if(viewChangeEvent instanceof FaqContentEvent) {
        this.faqContentList = (<FaqContentEvent>viewChangeEvent).faqContent;
      }
    });
    this._logger.logInfo('component created');
  }

  public goTo(location: string): void {
    window.location.hash = location;
  }

}
