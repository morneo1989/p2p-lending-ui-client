import {Component, OnInit} from "@angular/core";
import {User} from "../model/user/User";
import {Router} from "@angular/router";
import {UsersLoginService} from "../services/users.login.service";
import {SessionStorageService} from "../services/session.storage.service";
import {UsersAuthenticationManagerService} from "../services/users.authentication.manager.service";
import {UserType} from "../model/user/enums/user.type";
import {AppLoggerFactory} from "../utilities/app.logger.factory";

@Component({
  selector: 'div[investor-panel]',
  templateUrl: '../templates/investor-panel.html'
})
export class InvestorPanelComponent implements OnInit {

  private _logger = this._loggerFactory.getLogger('InvestorPanelComponent');

  public investor = new User();

  public constructor(private router: Router, private sessionStorage: SessionStorageService,
                     private loginService: UsersLoginService, private usersAuthenticationManager: UsersAuthenticationManagerService
    , private _loggerFactory: AppLoggerFactory) {
    loginService.loginUserChanged.subscribe((val: boolean) => this.investor = loginService.getLoggedInUser());
    this._logger.logInfo('InvestorPanelComponent created');
  }

  ngOnInit() {
    this.router.events.subscribe((val) => {
      if (val.url == '/pospo/investor/panel') {
        this._logger.logInfo("InvestorPanelComponent on router navigation: " + val);
        this.usersAuthenticationManager.getCurrentUserFromSession(
          (retrievedUser: User) => {
            if (retrievedUser == null || retrievedUser.token == null || retrievedUser.userType != UserType.INVESTOR) {
              if (this.loginService.getLoggedInUser().token == null || this.loginService.getLoggedInUser().userType != UserType.INVESTOR) {
                this.router.navigate(['/pospo']);
                return;
              }
            }
          },
          () => {
            //do nothing if login timeout
          });
      }
    });
  }

  public logoutUser() {
    this.sessionStorage.clearAuthenticatedUser((isError: boolean) => {
      if (isError) {
        this._logger.logInfo("Error while clearing user occurred");
      } else {
        this.loginService.setLoggedInUser(new User());
        this.router.navigate(['/pospo/logout']);
      }
    });
  }

}
