import {Component} from "@angular/core"
import {AppLoggerFactory} from "../utilities/app.logger.factory";

@Component({
	selector: 'promoted-auctions',
	templateUrl: '../templates/promoted_auctions.html'
})

export class PromotedAuctionsComponent {

  private _logger = this._loggerFactory.getLogger('PromotedAuctionsComponent');

	public constructor(private _loggerFactory: AppLoggerFactory) {
    this._logger.logInfo('component created')
	}
}
