import {Component} from "@angular/core";
import {AppLoggerFactory} from "../../utilities/app.logger.factory";
import {AboutUs} from "../../model/moderation/about.us";
import {ContentModerationPageController} from "../../controllers/content.moderation.controller";
import {AboutUsContentEvent} from "../../model/events/about.us.content.event";

@Component({
  selector: 'div[about-us-moderation]',
  templateUrl: '../../templates/moderation/about_us_moderation.html'
})
export class AboutUsModerationComponent {

  private _logger = this._loggerFactory.getLogger('AboutUsModerationComponent');

  private aboutUsToModerate: AboutUs;

  public aboutUsContent = `<p>Wprowadź treść opisu..</p>`;
  public editorConfig = {};
  public moderationErrorMessage: string;
  public saveOrUpdateDone: boolean;

  constructor(private controller: ContentModerationPageController, private _loggerFactory: AppLoggerFactory) {
    this.saveOrUpdateDone = false;
    this.controller.subscribeForAboutUsViewChangeEvents((viewChangeEvent) => {
      if (viewChangeEvent instanceof AboutUsContentEvent) {
        this.aboutUsToModerate = (<AboutUsContentEvent>viewChangeEvent).aboutUsContent;
        this.aboutUsContent = this.aboutUsToModerate.htmlFormattedText;
      }
    });
    this._logger.logInfo('component created')
  }

  public onContentSaveOrUpdate() {
    if (this.aboutUsToModerate == null) {
      this.aboutUsToModerate = new AboutUs();
    }
    this.aboutUsToModerate.htmlFormattedText = this.aboutUsContent;
    this.controller.saveOrUpdateAboutUs(this.aboutUsToModerate, (isDone: boolean) => {
      if (!isDone) {
        this.saveOrUpdateDone = false;
        this.moderationErrorMessage = 'Wystąpił błąd podczas zapisywania treści';
      } else {
        this.saveOrUpdateDone = true;
        this.moderationErrorMessage = null;
      }
    })
  }

}
