import {Component} from "@angular/core";
import {AppLoggerFactory} from "../../utilities/app.logger.factory";
import {Faq} from "../../model/moderation/faq";
import {ContentModerationPageController} from "../../controllers/content.moderation.controller";
import {FaqContentEvent} from "../../model/events/faq.content.event";

@Component({
  selector: 'div[faq-moderation]',
  templateUrl: '../../templates/moderation/faq_moderation.html'
})
export class FaqModerationComponent {

  private _logger = this._loggerFactory.getLogger('FaqModerationComponent');

  public faqQuestions: Faq[];
  public faqIdChosenToEdit: string;

  public faqQuestionContent = `Wprowadź treść pytania..`;
  public faqAnswerContent = `<p>Wprowadź treść odpowiedzi..</p>`;
  public editorConfig = {};
  public moderationErrorMessage: string;
  public saveOrUpdateDone: boolean;

  constructor(private controller: ContentModerationPageController, private _loggerFactory: AppLoggerFactory) {
    this.saveOrUpdateDone = false;
    this.controller.subscribeForFaqViewChangeEvents((viewChangeEvent) => {
      if (viewChangeEvent instanceof FaqContentEvent) {
        this.faqQuestions = (<FaqContentEvent>viewChangeEvent).faqContent;
        let addNewFaqElement = new Faq();
        addNewFaqElement._id = 'nonExistingId';
        addNewFaqElement.faqQuestion = 'Dodaj nowe pytanie FAQ';
        addNewFaqElement.htmlFormattedText = '<p>Wprowadź treść pytania..</p>';
        this.faqQuestions.push(addNewFaqElement);
        this.faqIdChosenToEdit = this.faqQuestions[0]._id;
        this.faqQuestionContent = this.faqQuestions[0].faqQuestion;
        this.faqAnswerContent = this.faqQuestions[0].htmlFormattedText;
      }
    });
    this._logger.logInfo('component created')
  }

  public onContentSaveOrUpdate() {
    this._logger.logDebug(JSON.stringify(this.faqQuestions));
    let faqsToSaveOrUpdate = [];
    this._logger.logDebug(JSON.stringify(faqsToSaveOrUpdate));
    this.faqQuestions.forEach((faq: Faq) => {
      if(faq._id != 'nonExistingId') {
        faqsToSaveOrUpdate.push(faq);
      }
      if((this.faqIdChosenToEdit != 'nonExistingId') && (faq._id == this.faqIdChosenToEdit)) {
        faq.faqQuestion = this.faqQuestionContent;
        faq.htmlFormattedText = this.faqAnswerContent;
      }
    });
    if(this.faqIdChosenToEdit == 'nonExistingId') {
      let newFaqToAdd = new Faq();
      newFaqToAdd.faqQuestion = this.faqQuestionContent;
      newFaqToAdd.htmlFormattedText = this.faqAnswerContent;
      faqsToSaveOrUpdate.push(newFaqToAdd);
    }
    this.controller.saveOrUpdateFaq(faqsToSaveOrUpdate, (isDone: boolean) => {
      if (!isDone) {
        this.saveOrUpdateDone = false;
        this.moderationErrorMessage = 'Wystąpił błąd podczas zapisywania pytań';
      } else {
        this.saveOrUpdateDone = true;
        this.moderationErrorMessage = null;
      }
    })
  }

  public onFaqToEditChosen() {
    if(this.faqIdChosenToEdit == 'nonExistingId') {
      this.faqQuestionContent = `<p>Wprowadź treść pytania..</p>`;
      this.faqAnswerContent = `<p>Wprowadź treść odpowiedzi..</p>`;
      return;
    }
    let indexOfFaq = this.faqQuestions.findIndex((faq: Faq) => faq._id == this.faqIdChosenToEdit);
    this.faqQuestionContent = this.faqQuestions[indexOfFaq].faqQuestion;
    this.faqAnswerContent = this.faqQuestions[indexOfFaq].htmlFormattedText;
  }

}
