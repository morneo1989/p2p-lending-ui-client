import {Component} from "@angular/core";
import {LegalRegulation} from "../../model/moderation/legal.regulation";
import {AppLoggerFactory} from "../../utilities/app.logger.factory";
import {ContentModerationPageController} from "../../controllers/content.moderation.controller";
import {LegalRegulationTitlesEvent} from "../../model/events/legal.regulation.titles.event";
import {FormGroup, FormBuilder, Validators, AbstractControl} from "@angular/forms";
import {FormsDataErrorStorage} from "../../configuration/forms.data.errors.storage";

@Component({
  selector: 'div[regulations-edit]',
  templateUrl: '../../templates/moderation/legal_regulations_moderation.html'
})
export class LegalRegulationsModerationComponent {

  private _logger = this._loggerFactory.getLogger('LegalRegulationsModerationComponent');

  public legalRegulations = [];

  public legalRegulationIdChosenToEdit: string;
  public legalRegulationContent: string;

  public legalRegulationEditForm: FormGroup;
  public legalRegulationEditErrors = {};

  public legalRegulationTitleForm: FormGroup;
  public legalRegulationTitleControl: AbstractControl;

  public legalRegulationSavedControl: AbstractControl;
  public legalRegulationUpdatedControl: AbstractControl;
  public legalRegulationSaveUnknownErrorControl: AbstractControl;
  public legalRegulationUpdateUnknownErrorControl: AbstractControl;
  public legalRegulationsGetUnknownErrorControl: AbstractControl;
  public legalRegulationGetUnknownErrorControl: AbstractControl;

  public editorConfig = {};

  constructor(private formBuilder: FormBuilder, private controller: ContentModerationPageController, private _loggerFactory: AppLoggerFactory,
              private errorsStorage: FormsDataErrorStorage) {
    this.legalRegulations = [];

    errorsStorage.subscribeForConfigurationLoadEvent((configuration) => {
      this.legalRegulationEditErrors = configuration.LEGAL_REGULATIONS;
    });

    this.legalRegulationEditForm = formBuilder.group({
      'legalRegulationSaved': false,
      'legalRegulationUpdated': false,
      'legalRegulationSaveUnknownError': false,
      'legalRegulationUpdateUnknownError': false,
      'legalRegulationsGetUnknownError': false,
      'legalRegulationGetUnknownError': false
    });
    this.legalRegulationSavedControl = this.legalRegulationEditForm.controls['legalRegulationSaved'];
    this.legalRegulationUpdatedControl = this.legalRegulationEditForm.controls['legalRegulationUpdated'];
    this.legalRegulationSaveUnknownErrorControl = this.legalRegulationEditForm.controls['legalRegulationSaveUnknownError'];
    this.legalRegulationUpdateUnknownErrorControl = this.legalRegulationEditForm.controls['legalRegulationUpdateUnknownError'];
    this.legalRegulationsGetUnknownErrorControl = this.legalRegulationEditForm.controls['legalRegulationsGetUnknownError'];
    this.legalRegulationGetUnknownErrorControl = this.legalRegulationEditForm.controls['legalRegulationGetUnknownError'];

    this.legalRegulationTitleForm = formBuilder.group({
      'legalRegulationTitle': ['', Validators.required]
    });
    this.legalRegulationTitleControl = this.legalRegulationTitleForm.controls['legalRegulationTitle'];

    this.controller.subscribeForLegalRegulationsViewChangeEvents((viewChangeEvent) => {
      if (viewChangeEvent instanceof LegalRegulationTitlesEvent) {
        let regulationTitles = (<LegalRegulationTitlesEvent>viewChangeEvent).regulationTitles;
        this._logger.logInfo(regulationTitles.length + ' legal regulation titles event');
        this.legalRegulations = regulationTitles;
        this.legalRegulations.push(new LegalRegulation('-1', 'Utwórz nowy regulamin...', '<p>Wprowadź treść regulaminu..</p>'));
        this.legalRegulationIdChosenToEdit = this.legalRegulations[0]._id;
        if(regulationTitles.length > 0) {
          this.controller.getLegalRegulationText(this.legalRegulations[0]._id, (legalRegulation) => {
            this.legalRegulations[0].htmlFormattedText = legalRegulation.htmlFormattedText;
            this.legalRegulationContent = this.legalRegulations[0].htmlFormattedText;
          })
        } else {
          this.legalRegulationContent = this.legalRegulations[0].htmlFormattedText;
        }
      }
    });
    this._logger.logInfo('component created')
  }

  public legalRegulationSaveOrUpdateButtonEnabled(): boolean {
    return this.legalRegulationContent != null && this.legalRegulationContent != ''
      && this.legalRegulationContent != '<p>Wprowadź treść nowego regulaminu</p>\n';
  }

  public onLegalRegulationChosen(): void {
    this.legalRegulationEditForm.reset();
    if (this.legalRegulationIdChosenToEdit == '-1') {
      this.legalRegulationContent = '<p>Wprowadź treść nowego regulaminu</p>';
    } else {
      this.controller.getLegalRegulationText(this.legalRegulationIdChosenToEdit, (legalRegulation: LegalRegulation) => {
        this.legalRegulationContent = legalRegulation.htmlFormattedText;
      });
    }
  }

  public onLegalRegulationSaveOrUpdate(): void {
    if (this.legalRegulationIdChosenToEdit == '-1') {
      this.controller.saveLegalRegulation(new LegalRegulation(null, this.legalRegulationTitleControl.value, this.legalRegulationContent), (isSaved) => {
        if (!isSaved) {
          this.legalRegulationSaveUnknownErrorControl.setValue(true);
        } else {
          this.legalRegulationEditForm.reset();
          this.legalRegulationSavedControl.setValue(true);
          this.reloadLegalRegulationsList();
          this.onLegalRegulationChosen();
        }
      })
    } else {
      let chosenLegalRegulationTitle = "";
      this.legalRegulations.forEach((regulation) => {
        if (regulation._id == this.legalRegulationIdChosenToEdit) {
          chosenLegalRegulationTitle = regulation.title;
        }
      });
      this.controller.updateLegalRegulation(new LegalRegulation(this.legalRegulationIdChosenToEdit, chosenLegalRegulationTitle, this.legalRegulationContent), (isUpdated) => {
        if (!isUpdated) {
          this.legalRegulationUpdateUnknownErrorControl.setValue(true);
        } else {
          this.legalRegulationEditForm.reset();
          this.legalRegulationUpdatedControl.setValue(true);
          this.reloadLegalRegulationsList();
          this.onLegalRegulationChosen();
        }
      });
    }
  }

  public legalRegulationsExists(): boolean {
    return this.legalRegulations.length > 1;
  }

  public isNewLegalRegulationActionChosen(): boolean {
    return this.legalRegulationIdChosenToEdit == '-1' || this.legalRegulations.length == 1;
  }

  private reloadLegalRegulationsList(): void {
    this.legalRegulations = [];
    this.controller.getLegalRegulationsTitles((regulationTitles: LegalRegulation[]) => {
      this._logger.logDebug('loaded ' + regulationTitles.length + ' legal moderation');
      regulationTitles.forEach((legalRegulation: LegalRegulation) => {
        this.legalRegulations.push(legalRegulation);
      });
      this.legalRegulations.push(new LegalRegulation('-1', 'Utwórz nowy regulamin..', '<p>Wprowadź treść regulaminu..</p>'));
      this.legalRegulationContent = this.legalRegulations[0].htmlFormattedText;
      this.legalRegulationIdChosenToEdit = this.legalRegulations[0]._id;
    });
  }

}
