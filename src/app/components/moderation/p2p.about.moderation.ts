import {Component} from "@angular/core";
import {AppLoggerFactory} from "../../utilities/app.logger.factory";
import {P2PAbout} from "../../model/moderation/p2p.about";
import {ContentModerationPageController} from "../../controllers/content.moderation.controller";
import {P2PAboutContentEvent} from "../../model/events/p2p.about.content.event";

@Component({
  selector: 'div[p2p-about-moderation]',
  templateUrl: '../../templates/moderation/p2p_about_moderation.html'
})
export class P2PAboutModerationComponent {

  private _logger = this._loggerFactory.getLogger('P2PAboutModerationComponent');

  private p2pAboutToModerate: P2PAbout;

  public p2pAboutContent = `<p>Wprowadź treść opisu..</p>`;
  public editorConfig = {};
  public moderationErrorMessage: string;
  public saveOrUpdateDone: boolean;

  constructor(private controller: ContentModerationPageController, private _loggerFactory: AppLoggerFactory) {
    this.saveOrUpdateDone = false;
    this.controller.subscribeForP2PAboutViewChangeEvents((viewChangeEvent) => {
      if (viewChangeEvent instanceof P2PAboutContentEvent) {
        this.p2pAboutToModerate = (<P2PAboutContentEvent>viewChangeEvent).p2pAboutContent;
        this.p2pAboutContent = this.p2pAboutToModerate.htmlFormattedText;
      }
    });
    this._logger.logInfo('component created')
  }

  public onContentSaveOrUpdate() {
    if (this.p2pAboutToModerate == null) {
      this.p2pAboutToModerate = new P2PAbout();
    }
    this.p2pAboutToModerate.htmlFormattedText = this.p2pAboutContent;
    this.controller.saveOrUpdateBorrowGuide(this.p2pAboutToModerate, (isDone: boolean) => {
      if (!isDone) {
        this.saveOrUpdateDone = false;
        this.moderationErrorMessage = 'Wystąpił błąd podczas zapisywania treści';
      } else {
        this.saveOrUpdateDone = true;
        this.moderationErrorMessage = null;
      }
    })
  }

}
