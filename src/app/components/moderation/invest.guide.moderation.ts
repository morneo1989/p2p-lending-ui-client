import {Component} from "@angular/core";
import {AppLoggerFactory} from "../../utilities/app.logger.factory";
import {InvestGuide} from "../../model/moderation/invest.guide";
import {ContentModerationPageController} from "../../controllers/content.moderation.controller";
import {InvestGuideContentEvent} from "../../model/events/invest.guide.content.event";

@Component({
  selector: 'div[invest-guide-moderation]',
  templateUrl: '../../templates/moderation/invest_guide_moderation.html'
})
export class InvestGuideModerationComponent {

  private _logger = this._loggerFactory.getLogger('ContactModerationComponent');

  private investGuideToModerate: InvestGuide;

  public investGuideContent = `<p>Wprowadź treść opisu..</p>`;
  public editorConfig = {};
  public moderationErrorMessage: string;
  public saveOrUpdateDone: boolean;

  constructor(private controller: ContentModerationPageController, private _loggerFactory: AppLoggerFactory) {
    this.saveOrUpdateDone = false;
    this.controller.subscribeForInvestGuideViewChangeEvents((viewChangeEvent) => {
      if (viewChangeEvent instanceof InvestGuideContentEvent) {
        this.investGuideToModerate = (<InvestGuideContentEvent>viewChangeEvent).investGuideContent;
        this.investGuideContent = this.investGuideToModerate.htmlFormattedText;
      }
    });
    this._logger.logInfo('component created')
  }

  public onContentSaveOrUpdate() {
    if (this.investGuideToModerate == null) {
      this.investGuideToModerate = new InvestGuide();
    }
    this.investGuideToModerate.htmlFormattedText = this.investGuideContent;
    this.controller.saveOrUpdateBorrowGuide(this.investGuideToModerate, (isDone: boolean) => {
      if (!isDone) {
        this.saveOrUpdateDone = false;
        this.moderationErrorMessage = 'Wystąpił błąd podczas zapisywania treści';
      } else {
        this.saveOrUpdateDone = true;
        this.moderationErrorMessage = null;
      }
    })
  }

}
