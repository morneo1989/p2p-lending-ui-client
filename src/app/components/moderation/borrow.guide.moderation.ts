import {Component} from "@angular/core";
import {AppLoggerFactory} from "../../utilities/app.logger.factory";
import {BorrowGuide} from "../../model/moderation/borrow.guide";
import {ContentModerationPageController} from "../../controllers/content.moderation.controller";
import {BorrowGuideContentEvent} from "../../model/events/borrow.guide.content.event";

@Component({
  selector: 'div[borrow-guide-moderation]',
  templateUrl: '../../templates/moderation/borrow_guide_moderation.html'
})
export class BorrowGuideModerationComponent {

  private _logger = this._loggerFactory.getLogger('BorrowGuideModerationComponent');

  private borrowGuideToModerate: BorrowGuide;

  public borrowGuideContent = `<p>Wprowadź treść opisu..</p>`;
  public editorConfig = {};
  public moderationErrorMessage: string;
  public saveOrUpdateDone: boolean;

  constructor(private controller: ContentModerationPageController, private _loggerFactory: AppLoggerFactory) {
    this.saveOrUpdateDone = false;
    this.controller.subscribeForBorrowGuideViewChangeEvents((viewChangeEvent) => {
      if (viewChangeEvent instanceof BorrowGuideContentEvent) {
        this.borrowGuideToModerate = (<BorrowGuideContentEvent>viewChangeEvent).borrowGuideContent;
        this.borrowGuideContent = this.borrowGuideToModerate.htmlFormattedText;
      }
    });
    this._logger.logInfo('component created')
  }

  public onContentSaveOrUpdate() {
    if (this.borrowGuideToModerate == null) {
      this.borrowGuideToModerate = new BorrowGuide();
    }
    this.borrowGuideToModerate.htmlFormattedText = this.borrowGuideContent;
    this.controller.saveOrUpdateBorrowGuide(this.borrowGuideToModerate, (isDone: boolean) => {
      if (!isDone) {
        this.saveOrUpdateDone = false;
        this.moderationErrorMessage = 'Wystąpił błąd podczas zapisywania treści';
      } else {
        this.saveOrUpdateDone = true;
        this.moderationErrorMessage = null;
      }
    })
  }

}
