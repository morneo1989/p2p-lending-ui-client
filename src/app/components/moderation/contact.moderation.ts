import {Component} from "@angular/core";
import {AppLoggerFactory} from "../../utilities/app.logger.factory";
import {Contact} from "../../model/moderation/contact";
import {ContentModerationPageController} from "../../controllers/content.moderation.controller";
import {ContactContentEvent} from "../../model/events/contact.content.event";

@Component({
  selector: 'div[contact-moderation]',
  templateUrl: '../../templates/moderation/contact_moderation.html'
})
export class ContactModerationComponent {

  private _logger = this._loggerFactory.getLogger('ContactModerationComponent');

  private contactToModerate: Contact;

  public contactContent = `<p>Wprowadź treść opisu..</p>`;
  public editorConfig = {};
  public moderationErrorMessage: string;
  public saveOrUpdateDone: boolean;

  constructor(private controller: ContentModerationPageController, private _loggerFactory: AppLoggerFactory) {
    this.saveOrUpdateDone = false;
    this.controller.subscribeForContactViewChangeEvents((viewChangeEvent) => {
      if (viewChangeEvent instanceof ContactContentEvent) {
        this.contactToModerate = (<ContactContentEvent>viewChangeEvent).contactContent;
        this.contactContent = this.contactToModerate.htmlFormattedText;
      }
    });
    this._logger.logInfo('component created')
  }

  public onContentSaveOrUpdate() {
    if (this.contactToModerate == null) {
      this.contactToModerate = new Contact();
    }
    this.contactToModerate.htmlFormattedText = this.contactContent;
    this.controller.saveOrUpdateBorrowGuide(this.contactToModerate, (isDone: boolean) => {
      if (!isDone) {
        this.saveOrUpdateDone = false;
        this.moderationErrorMessage = 'Wystąpił błąd podczas zapisywania treści';
      } else {
        this.saveOrUpdateDone = true;
        this.moderationErrorMessage = null;
      }
    })
  }

}
