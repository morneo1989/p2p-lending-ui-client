import {Component} from "@angular/core"
import {AppLoggerFactory} from "../utilities/app.logger.factory";

@Component({
	selector: 'partners',
	templateUrl: '../templates/partners.html'
})

export class PartnersComponent {

  private _logger = this._loggerFactory.getLogger('PartnersComponent');

	public constructor(private _loggerFactory: AppLoggerFactory) {
    this._logger.logInfo('component created')
	}
}
