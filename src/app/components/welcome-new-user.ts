import {Component, OnInit} from '@angular/core';
import {Subscription} from "rxjs";
import {ActivatedRoute} from "@angular/router";
import {UsersLoginService} from "../services/users.login.service";
import {AppLoggerFactory} from "../utilities/app.logger.factory";

@Component({
  selector: 'div[welcome-new-user]',
  templateUrl: '../templates/welcome-new-user.html'
})
export class WelcomeNewUserComponent implements OnInit {

  private _logger = this._loggerFactory.getLogger('WelcomeNewUserComponent');

  public registrationResponseHeader: string;
  public registrationResponseMessage: string;

  private subscription: Subscription;

  constructor(private activatedRoute: ActivatedRoute, public loginService: UsersLoginService, private _loggerFactory: AppLoggerFactory) {
    this.registrationResponseHeader = 'Proces rejestracji został zakończony';
    this.registrationResponseMessage = 'W celu korzystania z naszej platformy prosimy o zalogowanie do twojego panelu użytkownika';
    this._logger.logInfo('component created')
  }

  ngOnInit() {
    // subscribe to router event
    this.subscription = this.activatedRoute.queryParams.subscribe(
      (param: any) => {
        let response = param['response'];
        this._logger.logInfo(response);
        if (response == 'ok') {
          this.registrationResponseHeader = 'Proces rejestracji został zakończony';
          this.registrationResponseMessage = 'W celu korzystania z naszej platformy prosimy o zalogowanie do twojego panelu użytkownika';
        } else if (response == 'error') {
          this.registrationResponseHeader = 'Weryfikacja adresu email zakończyła się błędem';
          this.registrationResponseMessage = 'W trakcie procesu aktywacji adresu email wystąpił błąd';
        }

      });
  }

  ngOnDestroy() {
    // prevent memory leak by unsubscribing
    this.subscription.unsubscribe();
  }
}
