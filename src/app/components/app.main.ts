import {Component} from "@angular/core";
import {AppLoggerFactory} from "../utilities/app.logger.factory";
import {UsersAuthenticationManagerService} from "../services/users.authentication.manager.service";
import {User} from "../model/user/User";
import {Router, NavigationEnd} from "@angular/router";
import {isNullOrUndefined} from "util";
import {ApplicationDataSynchronizationService} from "../services/application.data.synchronization.service";

@Component({
  selector: 'body[main]',
  templateUrl: '../templates/main.html'
})
export class AppMainPageComponent {

  private _logger = this._loggerFactory.getLogger('AppMainPageComponent');

  public constructor(private router: Router, private usersAuthenticationManager: UsersAuthenticationManagerService, private dateSharedStorage: ApplicationDataSynchronizationService,
                     private _loggerFactory: AppLoggerFactory) {
    router.events.subscribe((event: NavigationEnd) => {
      if(event instanceof NavigationEnd) {
        this._logger.logInfo('Navigating to: ' + event.url);
        if (event.url != '/pospo' && (event.url.match(/pospo\/faq.*/i) == null) && (event.url.match(/pospo\/info\?contentType=.*/i) == null)) {
          this.usersAuthenticationManager.getCurrentUserFromSession(
            (retrievedUser: User) => {
              if (isNullOrUndefined(retrievedUser) || isNullOrUndefined(retrievedUser.token)) {
                this.router.navigate(['/pospo']);
              } else {
                dateSharedStorage.setLoggedInUser(retrievedUser);
              }
            },
            () => {
              this.router.navigate(['/pospo']);
            });
        }
      }
    });
    this._logger.logInfo('component created');
  }
}
