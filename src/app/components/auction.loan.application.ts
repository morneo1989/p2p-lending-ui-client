import {Component, OnInit} from "@angular/core";
import {User} from "../model/user/User";
import {Router} from "@angular/router";
import {UsersLoginService} from "../services/users.login.service";
import {SessionStorageService} from "../services/session.storage.service";
import {UsersAuthenticationManagerService} from "../services/users.authentication.manager.service";
import {UserType} from "../model/user/enums/user.type";
import {AppLoggerFactory} from "../utilities/app.logger.factory";
import {FormGroup, AbstractControl, FormBuilder, Validators} from "@angular/forms";
import {AuctionLoanApplicationPageController} from "../controllers/auction.loan.application.controller";
import {FormsDataErrorStorage} from "../configuration/forms.data.errors.storage";
import {FormsDataInputStorage} from "../configuration/forms.data.input.storage";
import {Auction} from "../model/auction/auction";

@Component({
  selector: 'div[auction-loan-application]',
  templateUrl: '../templates/auction-loan-application.html'
})
export class AuctionLoanApplicationPanelComponent {

  private _logger = this._loggerFactory.getLogger('BorrowerAuctionsListPanelComponent');

  public newAuctionForm: FormGroup;

  public auctionTitleControl: AbstractControl;
  public auctionDescriptionControl: AbstractControl;
  public auctionAmountControl: AbstractControl;
  public auctionPayoffPeriodControl: AbstractControl;
  public loanTargetControl: AbstractControl;

  public calculatedLoanInterestControl: AbstractControl;

  public loanTargets = [];

  public auctionCreationError = {};

  public auctionSaveError = false;
  public auctionSaveSuccess = false;

  public constructor(private controller: AuctionLoanApplicationPageController, private _loggerFactory: AppLoggerFactory, private inputStorage: FormsDataInputStorage,
                     private errorsStorage: FormsDataErrorStorage, private formBuilder: FormBuilder) {

    errorsStorage.subscribeForConfigurationLoadEvent((configuration) => {
      this.auctionCreationError = configuration.LOANS;
    });

    inputStorage.subscribeForConfigurationLoadEvent((configuration) => {
      this.loanTargets = configuration.LOAN_TARGETS;
    });

    this.newAuctionForm = formBuilder.group({
      'auctionTitle': ['', Validators.required],
      'auctionDescription': [''],
      'auctionAmount': ['', [Validators.required, Validators.pattern('[0-9]*')]],
      'auctionPayoffPeriod': ['', [Validators.required, Validators.pattern('[0-9]*')]],
      'loanTarget': ['', Validators.required],
      'calculatedLoanInterest': 0.0
    });

    this.auctionTitleControl = this.newAuctionForm.controls['auctionTitle'];
    this.auctionDescriptionControl = this.newAuctionForm.controls['auctionDescription'];
    this.auctionAmountControl = this.newAuctionForm.controls['auctionAmount'];
    this.auctionPayoffPeriodControl = this.newAuctionForm.controls['auctionPayoffPeriod'];
    this.loanTargetControl = this.newAuctionForm.controls['loanTarget'];

    this.calculatedLoanInterestControl = this.newAuctionForm.controls['calculatedLoanInterest'];

    this.controller.subscribeForViewChangeEvents((viewChangeEvent) => {

    });

    this._logger.logInfo('component created')
  }

  public onNewAuctionSaveClick(): void {
    let newAuction = new Auction();
    newAuction.title = this.auctionTitleControl.value;
    newAuction.description  = this.auctionDescriptionControl.value;
    newAuction.loanAmount  = this.auctionAmountControl.value;
    newAuction.payoffPeriodInMonths = this.auctionPayoffPeriodControl.value;
    newAuction.rateOfLoan = ((this.calculatedLoanInterestControl.value + 100)*newAuction.loanAmount)/(100*newAuction.payoffPeriodInMonths);
    newAuction.loanTarget = this.loanTargetControl.value;
    this.controller.saveNewAuction(newAuction, (isSaved: boolean) => {
      this.auctionSaveError = !isSaved;
      this.auctionSaveSuccess = isSaved;
    });
  }


}
