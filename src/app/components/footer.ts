import {Component} from "@angular/core";
import {Router} from "@angular/router";
import {AppLoggerFactory} from "../utilities/app.logger.factory";
import {FooterPageController} from "../controllers/footer.controller";
import {LegalRegulationTitlesEvent} from "../model/events/legal.regulation.titles.event";

@Component({
  selector: 'pospo-footer',
  templateUrl: '../templates/pospo-footer.html'
})

export class FooterComponent {

  private _logger = this._loggerFactory.getLogger('FooterComponent');

  public legalRegulationsTitles = [];

  public constructor(private router: Router, private _loggerFactory: AppLoggerFactory, private controller: FooterPageController) {
    controller.subscribeForViewChangeEvents((viewChangeEvent) => {
      if(viewChangeEvent instanceof LegalRegulationTitlesEvent) {
        this.legalRegulationsTitles = (<LegalRegulationTitlesEvent>viewChangeEvent).regulationTitles;
      }
    });
    this._logger.logInfo('component created')
  }

  public onLegalRegulationClicked(regulationIndex: number) {
    this.router.navigate(['/pospo/info'], {queryParams: {contentType: 'LEGAL_REGULATION', id: this.legalRegulationsTitles[regulationIndex]._id}});
  }

}
