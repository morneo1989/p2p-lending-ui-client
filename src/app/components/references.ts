import {Component} from "@angular/core"
import {AppLoggerFactory} from "../utilities/app.logger.factory";

@Component({
	selector: 'references',
	templateUrl: '../templates/references.html'
})

export class ReferencesComponent {

  private _logger = this._loggerFactory.getLogger('ReferencesComponent');

	public constructor(private _loggerFactory: AppLoggerFactory) {
    this._logger.logInfo('component created')
	}
}
