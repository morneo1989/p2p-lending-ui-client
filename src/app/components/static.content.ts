import {Component} from "@angular/core";
import {Router, ActivatedRoute} from "@angular/router";
import {AppLoggerFactory} from "../utilities/app.logger.factory";
import {StaticContentPageController} from "../controllers/static.content.controller";
import {StaticContentType} from "../model/enum/static.content.type";
import {AboutUs} from "../model/moderation/about.us";
import {P2PAbout} from "../model/moderation/p2p.about";
import {BorrowGuide} from "../model/moderation/borrow.guide";
import {Contact} from "../model/moderation/contact";
import {InvestGuide} from "../model/moderation/invest.guide";
import {Faq} from "../model/moderation/faq";
import {LegalRegulation} from "../model/moderation/legal.regulation";

@Component({
  selector: 'div[static-content]',
  templateUrl: '../templates/static_content.html'
})
export class StaticContentComponent {

  private _logger = this._loggerFactory.getLogger('StaticContentComponent');

  private contentType: StaticContentType;

  public staticContentTitle: string;
  public staticContent: string;

  public constructor(private route: ActivatedRoute, private router: Router, private _loggerFactory: AppLoggerFactory,
                     private controller: StaticContentPageController) {
    this.route.queryParams.subscribe((params) => {
      this._logger.logDebug('static content type: ' + params['contentType']);
      this.contentType = params['contentType'];
      switch (this.contentType) {
        case StaticContentType.ABOUT_US:
          controller.getAboutUsContent((content: AboutUs) => {
            this.staticContentTitle = 'O Nas';
            this.staticContent = content.htmlFormattedText;
          });
          break;
        case StaticContentType.BORROW_GUIDE:
          controller.getBorrowGuideContent((content: BorrowGuide) => {
            this.staticContentTitle = 'Inwestowanie na platformie POSPO krok po kroku';
            this.staticContent = content.htmlFormattedText;
          });
          break;
        case StaticContentType.CONTACT:
          controller.getContactContent((content: Contact) => {
            this.staticContentTitle = 'Kontakt';
            this.staticContent = content.htmlFormattedText;
          });
          break;
        case StaticContentType.INVEST_GUIDE:
          controller.getInvestGuideContent((content: InvestGuide) => {
            this.staticContentTitle = 'Pożyczanie pieniędzy na platformie POSPO krok po kroku';
            this.staticContent = content.htmlFormattedText;
          });
          break;
        case StaticContentType.P2P_ABOUT:
          controller.getP2PAboutContent((content: P2PAbout) => {
            this.staticContentTitle = 'Czym są pożyczki P2P ?';
            this.staticContent = content.htmlFormattedText;
          });
          break;
        case StaticContentType.LEGAL_REGULATION:
          let regulationId = params['id'];
          controller.getLegalRegulationContent(regulationId, (legalRegulation: LegalRegulation) => {
            this.staticContentTitle = legalRegulation.title;
            this.staticContent = legalRegulation.htmlFormattedText;
          });
          break;
      }
    });
    this._logger.logInfo('component created');
  }

}
