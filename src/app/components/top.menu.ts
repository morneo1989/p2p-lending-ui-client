import {Component} from "@angular/core"
import {Router} from '@angular/router';
import {User} from "../model/user/User";
import {UsersLoginService} from "../services/users.login.service";

import {SessionStorageService} from "../services/session.storage.service";
import {UserType} from "../model/user/enums/user.type";
import {FormGroup, FormBuilder, Validators, AbstractControl} from "@angular/forms";
import {FormsDataErrorStorage} from "../configuration/forms.data.errors.storage";
import {AppLoggerFactory} from "../utilities/app.logger.factory";
import {UserLoginResponse} from "../model/user/UserLoginResponse";
import {ApplicationDataSynchronizationService} from "../services/application.data.synchronization.service";

@Component({
  selector: 'div[top-menu]',
  templateUrl: '../templates/top_menu.html',
  styleUrls: ['../../resources/css/top_menu.css']
})

export class TopMenuComponent {

  private _logger = this._loggerFactory.getLogger('TopMenuComponent');

  public loggedInUser: User;

  public loginErrors = {};

  public userToLogin = new User("", "");
  public loginForm: FormGroup;

  public avatarDetailsVisible = false;

  public emailControl: AbstractControl;
  public passwordControl: AbstractControl;

  public constructor(private router: Router, private formBuilder: FormBuilder, private loginService: UsersLoginService, private sessionStorage: SessionStorageService,
                     private errorsStorage: FormsDataErrorStorage, private _loggerFactory: AppLoggerFactory, private dataSharedStorage: ApplicationDataSynchronizationService) {
    this.loggedInUser = dataSharedStorage.loggedInUser;
    dataSharedStorage.loggedInUserSubject.subscribe((loggedInUser: User) => {
      this._logger.logDebug('Logged in user: ' + JSON.stringify(loggedInUser));
      this.loggedInUser = loggedInUser;
    });

    errorsStorage.subscribeForConfigurationLoadEvent((configuration) => {
      this.loginErrors = configuration.LOGIN;
    });

    loginService.loginUserChanged.subscribe((val: boolean) => this.loggedInUser = loginService.getLoggedInUser());
    this.loginForm = formBuilder.group({
      'email': ['', [Validators.required, Validators.pattern('[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$')]],
      'password': ['', Validators.required],
    });
    this.emailControl = this.loginForm.controls['email'];
    this.passwordControl = this.loginForm.controls['password'];
    this._logger.logInfo('component created')
  }

  public isUserLoggedIn(): boolean {
    return this.loggedInUser.token != null;
  }

  public loginUser() {
    this.avatarDetailsVisible = false;
    this.userToLogin.email = this.emailControl.value;
    this.userToLogin.password = this.passwordControl.value;
    this.loginService.loginUser(this.userToLogin, (userLoginResponse: UserLoginResponse) => {
      this._logger.logInfo('User login response : ' + JSON.stringify(userLoginResponse));
      if(userLoginResponse.isError()) {
        if (userLoginResponse.passwordInvalid) {
          this.emailControl.clearValidators();
          this.passwordControl.clearValidators();
          this.passwordControl.setErrors({
            "invalid": true
          });
        } else if (userLoginResponse.unknownError) {
          this.emailControl.clearValidators();
          this.passwordControl.clearValidators();
          this.emailControl.setErrors({
            "unknown": true
          });
        } else if (userLoginResponse.userUnknown) {
          this.emailControl.clearValidators();
          this.passwordControl.clearValidators();
          this.emailControl.setErrors({
            "unknownError": true
          });
        }
      } else {
        this.loginForm.reset();
        this.userToLogin = new User();
        this.loggedInUser = this.userToLogin;
        if(userLoginResponse.userType == 'INVESTOR') {
          this.loggedInUser.userType = UserType.INVESTOR;
        } else if(userLoginResponse.userType == 'BORROWER') {
          this.loggedInUser.userType = UserType.BORROWER;
        } else {
          this.loggedInUser.userType = UserType.ADMIN;
        }
        this.loggedInUser.name = userLoginResponse.name;
        this.loggedInUser.surname = userLoginResponse.surname;
        this.loggedInUser.password = null;
        this.loggedInUser.repeatedPassword = null;
        this.loggedInUser.token = userLoginResponse.token;
        this.loginService.setLoggedInUser(this.loggedInUser);
        this.dataSharedStorage.setLoggedInUser(this.loggedInUser);
        this.sessionStorage.saveAuthenticatedUser(this.loggedInUser, (isError: boolean) => {
          if (isError) {
            console.log("Error while saving user occurred");
          } else {
            if (this.loggedInUser.userType == UserType.BORROWER) {
              this.router.navigate(['/pospo/borrower/panel']);
            } else if (this.loggedInUser.userType == UserType.INVESTOR) {
              this.router.navigate(['/pospo/investor/panel']);
            } else if (this.loggedInUser.userType == UserType.ADMIN) {
              this.router.navigate(['/pospo/admin/panel']);
            } else {
              this.passwordControl.setErrors({
                "typeUnknown": true
              });
            }
          }
        });
      }

    });
  }

  public logoutUser() {
    this.sessionStorage.clearAuthenticatedUser((isError: boolean) => {
      if (isError) {
        this._logger.logInfo("Error while clearing user occurred");
      } else {
        this.loginService.setLoggedInUser(new User());
        this.router.navigate(['/pospo/logout']);
      }
    });
  }

  public displayUserPanel() {
    switch (this.loggedInUser.userType) {
      case UserType.BORROWER:
        this.router.navigate(['/pospo/borrower/panel']);
        break;
      case UserType.INVESTOR:
        this.router.navigate(['/pospo/investor/panel']);
        break;
      case UserType.ADMIN:
        this.router.navigate(['/pospo/admin/panel']);
        break;
    }
  }

  public displayAvatarDetails() {
    this.avatarDetailsVisible = !this.avatarDetailsVisible;
  }

}
