import {Component} from "@angular/core";
import {User} from "../model/user/User";
import {Auction} from "../model/auction/auction";
import {UserRegisterResponse} from "../model/user/UserRegisterResponse";
import {PhoneVerifyResponse} from "../model/user/PhoneVerifyResponse";
import {UserType} from "../model/user/enums/user.type";
import {FormGroup, FormBuilder, Validators, AbstractControl} from "@angular/forms";
import {FormsDataInputStorage} from "../configuration/forms.data.input.storage";
import {FormsDataErrorStorage} from "../configuration/forms.data.errors.storage";
import {AppLoggerFactory} from "../utilities/app.logger.factory";
import {RegistrationPanelPageController} from "../controllers/registration.controller";
import {isNullOrUndefined} from "util";

@Component({
  selector: 'div[pre-registration]',
  templateUrl: '../templates/user_register.html',
  styleUrls: ['../../resources/css/user_pre_register.css']
})
export class RegistrationComponent {

  private _logger = this._loggerFactory.getLogger('RegistrationComponent');

  public registrationForm: FormGroup;

  public registrationTitle: string;

  public investorChosenTypeControl: AbstractControl;
  public borrowerChosenTypeControl: AbstractControl;

  public fullRegistrationCheckboxControl: AbstractControl;
  public emailAlreadyExistsControl: AbstractControl;
  public unknownErrorControl: AbstractControl;
  public unknownErrorPhoneVerificationControl: AbstractControl;
  public phoneVerificationErrorControl: AbstractControl;

  public calculatedLoanInterestControl: AbstractControl;

  public investorNameControl: AbstractControl;
  public investorSurnameControl: AbstractControl;
  public investorEmailControl: AbstractControl;
  public investorPhoneNumberControl: AbstractControl;
  public investorPasswordControl: AbstractControl;
  public investorRepeatedPasswordControl: AbstractControl;

  public borrowerNameControl: AbstractControl;
  public borrowerSurnameControl: AbstractControl;
  public borrowerEmailControl: AbstractControl;
  public borrowerPhoneNumberControl: AbstractControl;
  public borrowerPasswordControl: AbstractControl;
  public borrowerRepeatedPasswordControl: AbstractControl;

  public loanApplicationCheckboxControl: AbstractControl;

  public ageControl: AbstractControl;
  public currentDebtControl: AbstractControl;
  public averageSalaryControl: AbstractControl;
  public maritalStatusPeriodControl: AbstractControl;
  public employmentAreaControl: AbstractControl;
  public incomeSourceControl: AbstractControl;
  public maritalStatusControl: AbstractControl;
  public numberOfPeopleDependentControl: AbstractControl;

  public companyNameControl: AbstractControl;
  public companyTaxIdControl: AbstractControl;
  public companyStreetNameControl: AbstractControl;
  public companyStreetNumberControl: AbstractControl;
  public companyFlatNumberControl: AbstractControl;
  public companyPostalCodeControl: AbstractControl;
  public companyCityControl: AbstractControl;
  public companyCorrespondenceStreetNameControl: AbstractControl;
  public companyCorrespondenceStreetNumberControl: AbstractControl;
  public companyCorrespondenceFlatNumberControl: AbstractControl;
  public companyCorrespondencePostalCodeControl: AbstractControl;
  public companyCorrespondenceCityControl: AbstractControl;

  public companyFormControl: AbstractControl;
  public companyDifferentCorrespondenceAddressControl: AbstractControl;

  public auctionTitleControl: AbstractControl;
  public auctionDescriptionControl: AbstractControl;
  public auctionAmountControl: AbstractControl;
  public auctionPayoffPeriodControl: AbstractControl;
  public loanTargetControl: AbstractControl;

  public agreementContractControl: AbstractControl;
  public agreementMarketingControl: AbstractControl;

  public phoneVerificationCodeControl: AbstractControl;

  public userRegistrationError = {};
  public auctionCreationError = {};

  public loanTargets = [];
  public employmentAreas = [];
  public incomeSources = [];
  public maritalStatuses = [];
  public companyForms = [];
  public phoneVerificationSendCompleted: boolean;
  public registrationCompleted: boolean;

  constructor(private formBuilder: FormBuilder, private inputStorage: FormsDataInputStorage,
              private errorsStorage: FormsDataErrorStorage, private _loggerFactory: AppLoggerFactory,
              private controller: RegistrationPanelPageController) {

    errorsStorage.subscribeForConfigurationLoadEvent((configuration) => {
      this.userRegistrationError = configuration.REGISTRATION;
      this.auctionCreationError = configuration.LOANS;
    });

    inputStorage.subscribeForConfigurationLoadEvent((configuration) => {
      this.loanTargets = configuration.LOAN_TARGETS;
      this.incomeSources = configuration.INCOME_SOURCES;
      this.maritalStatuses = configuration.MARITAL_STATUSES;
      this.companyForms = configuration.COMPANY_FORMS;
      this.employmentAreas = configuration.EMPLOYMENT_AREAS;
    });

    this.registrationCompleted = false;
    this.registrationTitle = 'Rejestracja nowego użytkownika';

    this.registrationForm = formBuilder.group({
      'investorTypeRadio': false,
      'borrowerTypeRadio': false,
      'fullRegistrationCheckbox': false,
      'emailAlreadyExists': false,
      'unknownError': false,
      'unknownErrorPhoneVerification': false,
      'phoneVerificationError': false,
      'loanApplicationCheckbox': false,

      'calculatedLoanInterest': 0.0,

      'investorName': ['', [Validators.required, Validators.pattern('[a-zA-Z]*')]],
      'investorSurname': ['', [Validators.required, Validators.pattern('[a-zA-Z]*')]],
      'investorEmail': ['', [Validators.required, Validators.pattern('/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/')]],
      'investorPhoneNumber': ['', [Validators.required, Validators.pattern('[0-9]*')]],
      'investorPassword': ['', Validators.required],
      'investorRepeatedPassword': ['', Validators.required],

      'borrowerName': ['', [Validators.required, Validators.pattern('[a-zA-Z]*')]],
      'borrowerSurname': ['', [Validators.required, Validators.pattern('[a-zA-Z]*')]],
      'borrowerEmail': ['', [Validators.required, Validators.pattern('/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/')]],
      'borrowerPhoneNumber': ['', [Validators.required, Validators.pattern('[0-9]*')]],
      'borrowerPassword': ['', Validators.required],
      'borrowerRepeatedPassword': ['', Validators.required],

      'employmentArea': ['', Validators.required],
      'incomeSource': ['', Validators.required],
      'maritalStatus': ['', Validators.required],
      'numberOfPeopleDependent': ['', Validators.required],

      'age': ['', [Validators.required, Validators.pattern('[0-9]*')]],
      'currentDebt': ['', [Validators.required, Validators.pattern('[0-9]*')]],
      'averageSalary': ['', [Validators.required, Validators.pattern('[0-9]*')]],
      'maritalStatusPeriod': ['', [Validators.required, Validators.pattern('[0-9]*')]],

      'companyName': ['', [Validators.required, Validators.pattern('[a-zA-Z]*')]],
      'companyTaxId': ['', [Validators.required, Validators.pattern('[0-9]{10}')]],
      'companyStreetName': ['', [Validators.required, Validators.pattern('[a-zA-Z]*')]],
      'companyStreetNumber': ['', [Validators.required, Validators.pattern('[0-9]*')]],
      'companyFlatNumber': ['', [Validators.required, Validators.pattern('[0-9]*')]],
      'companyPostalCode': ['', [Validators.required, Validators.pattern('[0-9]{2}-[0-9]{3}')]],
      'companyCity': ['', Validators.required],
      'companyCorrespondenceStreetName': ['', [Validators.required, Validators.pattern('[a-zA-Z]*')]],
      'companyCorrespondenceStreetNumber': ['', [Validators.required, Validators.pattern('[0-9]*')]],
      'companyCorrespondenceFlatNumber': ['', [Validators.required, Validators.pattern('[0-9]*')]],
      'companyCorrespondencePostalCode': ['', [Validators.required, Validators.pattern('[0-9]{2}-[0-9]{3}')]],
      'companyCorrespondenceCity': ['', [Validators.required, Validators.pattern('[a-zA-Z]*')]],

      'companyForm': ['', Validators.required],
      'differentCompanyCorrespondenceAddress': false,

      'auctionTitle': ['', Validators.required],
      'auctionDescription': [''],
      'auctionAmount': ['', [Validators.required, Validators.pattern('[0-9]*')]],
      'auctionPayoffPeriod': ['', [Validators.required, Validators.pattern('[0-9]*')]],
      'loanTarget': ['', Validators.required],

      'agreementContract': ['', Validators.required],
      'agreementMarketing': ['', Validators.required],

      'phoneVerificationCode': ['']
    });

    this.fullRegistrationCheckboxControl = this.registrationForm.controls['fullRegistrationCheckbox'];
    this.emailAlreadyExistsControl = this.registrationForm.controls['emailAlreadyExists'];
    this.unknownErrorControl = this.registrationForm.controls['unknownError'];
    this.unknownErrorPhoneVerificationControl = this.registrationForm.controls['unknownErrorPhoneVerification'];
    this.phoneVerificationErrorControl = this.registrationForm.controls['phoneVerificationError'];

    this.calculatedLoanInterestControl = this.registrationForm.controls['calculatedLoanInterest'];

    this.investorChosenTypeControl = this.registrationForm.controls['investorTypeRadio'];
    this.investorChosenTypeControl.valueChanges.subscribe((value) =>
    {
      if(value) {
        this.registrationTitle = 'Rejestracja nowego inwestora';
      }
    });
    this.borrowerChosenTypeControl = this.registrationForm.controls['borrowerTypeRadio'];
    this.borrowerChosenTypeControl.valueChanges.subscribe((value) =>
    {
      if(value) {
        this.registrationTitle = 'Rejestracja nowego pożyczkobiorcy';
      }
    });

    this.investorNameControl = this.registrationForm.controls['investorName'];
    this.investorSurnameControl = this.registrationForm.controls['investorSurname'];
    this.investorEmailControl = this.registrationForm.controls['investorEmail'];
    this.investorPhoneNumberControl = this.registrationForm.controls['investorPhoneNumber'];
    this.investorPasswordControl = this.registrationForm.controls['investorPassword'];
    this.investorRepeatedPasswordControl = this.registrationForm.controls['investorRepeatedPassword'];

    this.borrowerNameControl = this.registrationForm.controls['borrowerName'];
    this.borrowerSurnameControl = this.registrationForm.controls['borrowerSurname'];
    this.borrowerEmailControl = this.registrationForm.controls['borrowerEmail'];
    this.borrowerPhoneNumberControl = this.registrationForm.controls['borrowerPhoneNumber'];
    this.borrowerPasswordControl = this.registrationForm.controls['borrowerPassword'];
    this.borrowerRepeatedPasswordControl = this.registrationForm.controls['borrowerRepeatedPassword'];

    this.loanApplicationCheckboxControl = this.registrationForm.controls['loanApplicationCheckbox'];

    this.ageControl = this.registrationForm.controls['age'];
    this.currentDebtControl = this.registrationForm.controls['currentDebt'];
    this.averageSalaryControl = this.registrationForm.controls['averageSalary'];
    this.maritalStatusPeriodControl = this.registrationForm.controls['maritalStatusPeriod'];
    this.employmentAreaControl = this.registrationForm.controls['employmentArea'];
    this.incomeSourceControl = this.registrationForm.controls['incomeSource'];
    this.maritalStatusControl = this.registrationForm.controls['maritalStatus'];
    this.numberOfPeopleDependentControl = this.registrationForm.controls['numberOfPeopleDependent'];

    this.companyNameControl = this.registrationForm.controls['companyName'];
    this.companyTaxIdControl = this.registrationForm.controls['companyTaxId'];
    this.companyStreetNameControl = this.registrationForm.controls['companyStreetName'];
    this.companyStreetNumberControl = this.registrationForm.controls['companyStreetNumber'];
    this.companyFlatNumberControl = this.registrationForm.controls['companyFlatNumber'];
    this.companyPostalCodeControl = this.registrationForm.controls['companyPostalCode'];
    this.companyCityControl = this.registrationForm.controls['companyCity'];
    this.companyCorrespondenceStreetNameControl = this.registrationForm.controls['companyCorrespondenceStreetName'];
    this.companyCorrespondenceStreetNumberControl = this.registrationForm.controls['companyCorrespondenceStreetNumber'];
    this.companyCorrespondenceFlatNumberControl = this.registrationForm.controls['companyCorrespondenceFlatNumber'];
    this.companyCorrespondencePostalCodeControl = this.registrationForm.controls['companyCorrespondencePostalCode'];
    this.companyCorrespondenceCityControl = this.registrationForm.controls['companyCorrespondenceCity'];

    this.companyFormControl = this.registrationForm.controls['companyForm'];
    this.companyDifferentCorrespondenceAddressControl = this.registrationForm.controls['differentCompanyCorrespondenceAddress'];

    this.auctionTitleControl = this.registrationForm.controls['auctionTitle'];
    this.auctionDescriptionControl = this.registrationForm.controls['auctionDescription'];
    this.auctionAmountControl = this.registrationForm.controls['auctionAmount'];
    this.auctionPayoffPeriodControl = this.registrationForm.controls['auctionPayoffPeriod'];
    this.loanTargetControl = this.registrationForm.controls['loanTarget'];

    this.agreementContractControl = this.registrationForm.controls['agreementContract'];
    this.agreementMarketingControl = this.registrationForm.controls['agreementMarketing'];

    this.phoneVerificationCodeControl = this.registrationForm.controls['phoneVerificationCode'];

    this.calculatedLoanInterestControl.setValue(5);

    this._logger.logInfo('component created');
  }

  public revertToUserTypeChoice() {
    this.investorChosenTypeControl.setValue(false);
    this.borrowerChosenTypeControl.setValue(false);
    this.registrationTitle = 'Rejestracja nowego użytkownika';
    this.fullRegistrationCheckboxControl.setValue(false);
    this.loanApplicationCheckboxControl.setValue(false);
    this.registrationForm.reset();
  }

  public companyCorrespondenceAddressDifferentThenRegistered(): boolean {
    return this.companyDifferentCorrespondenceAddressControl.value == true;
  }

  public onRegistrationContinue() {
    if (this.borrowerChosenTypeControl.value) {
      this.controller.sendPhoneVerificationRequest(this.borrowerEmailControl.value, this.borrowerPhoneNumberControl.value, (response: PhoneVerifyResponse) => {
        if (response == PhoneVerifyResponse.SUCCESS) {
          this._logger.logDebug('Sent phone verification request for email ' + this.borrowerEmailControl.value + ' and number ' + this.borrowerPhoneNumberControl.value);
          this.phoneVerificationSendCompleted = true;
        } else if (response == PhoneVerifyResponse.EMAIL_ALREADY_EXISTS) {
          this._logger.logDebug('Email ' + this.borrowerEmailControl.value + ' already exists');
          this.emailAlreadyExistsControl.setValue(true);
        } else {
          this._logger.logDebug('Unknown error occurred');
          this.unknownErrorControl.setValue(true);
        }
      });
    } else if (this.investorChosenTypeControl.value) {
      this.controller.sendPhoneVerificationRequest(this.investorEmailControl.value, this.investorPhoneNumberControl.value, (response: PhoneVerifyResponse) => {
        if (response == PhoneVerifyResponse.SUCCESS) {
          this._logger.logDebug('Sent phone verification request for email ' + this.borrowerEmailControl.value + ' and number ' + this.borrowerPhoneNumberControl.value);
          this.phoneVerificationSendCompleted = true;
        } else if (response == PhoneVerifyResponse.EMAIL_ALREADY_EXISTS) {
          this._logger.logDebug('Email ' + this.borrowerEmailControl.value + ' already exists');
          this.emailAlreadyExistsControl.setValue(true);
        } else {
          this._logger.logDebug('Unknown error occurred');
          this.unknownErrorControl.setValue(true);
        }
      });
    }
  }

  public onFinishRegistration() {
    if (!isNullOrUndefined(this.phoneVerificationCodeControl.value)) {
      let userToRegister = new User();
      userToRegister.email = this.borrowerEmailControl.value;
      userToRegister.password = this.borrowerPasswordControl.value;
      userToRegister.name = this.borrowerNameControl.value;
      userToRegister.surname = this.borrowerSurnameControl.value;
      userToRegister.phone = this.borrowerPasswordControl.value;
      if (this.borrowerChosenTypeControl.value) {
        userToRegister.userType = UserType.BORROWER;
        let auctionToCreate: Auction;
        if(this.loanApplicationCheckboxControl.value) {
          auctionToCreate = new Auction();
          auctionToCreate.title = this.auctionTitleControl.value;
          auctionToCreate.description = this.auctionTitleControl.value;
          auctionToCreate.loanAmount = this.auctionTitleControl.value;
          auctionToCreate.payoffPeriodInMonths = this.auctionTitleControl.value;
          auctionToCreate.rateOfLoan = ((this.calculatedLoanInterestControl.value + 100)*auctionToCreate.loanAmount)/(100*auctionToCreate.payoffPeriodInMonths);
          auctionToCreate.loanTarget = this.loanTargetControl.value;
        }
        if (this.fullRegistrationCheckboxControl.value) {
          userToRegister.ratingParameters.currentDebt = this.currentDebtControl.value;
          userToRegister.ratingParameters.age = this.ageControl.value;
          userToRegister.ratingParameters.employmentArea = this.employmentAreaControl.value;
          userToRegister.ratingParameters.incomeSource = this.incomeSourceControl.value;
          userToRegister.ratingParameters.maritalStatus = this.maritalStatusControl.value;
          userToRegister.ratingParameters.maritalStatusPeriodInMonths = this.maritalStatusPeriodControl.value;
          userToRegister.ratingParameters.monthlyAverageIncome = this.averageSalaryControl.value;
          userToRegister.ratingParameters.numberOfPeopleDependent = this.numberOfPeopleDependentControl.value;
          this.controller.registerFullBorrower(userToRegister, auctionToCreate, (response: UserRegisterResponse) => {
            if (response == UserRegisterResponse.SUCCESS) {
              this.phoneVerificationSendCompleted = false;
              this.registrationCompleted = true;
            } else if (response == UserRegisterResponse.PHONE_VERIFICATION_CODE_MISMATCH) {
              this.phoneVerificationErrorControl.setValue(true);
            } else {
              this.unknownErrorPhoneVerificationControl.setValue(true);
            }
          });
        } else {
          this.controller.registerBasicBorrower(userToRegister, auctionToCreate, (response: UserRegisterResponse) => {
            if (response == UserRegisterResponse.SUCCESS) {
              this.phoneVerificationSendCompleted = false;
              this.registrationCompleted = true;
            } else if (response == UserRegisterResponse.PHONE_VERIFICATION_CODE_MISMATCH) {
              this.phoneVerificationErrorControl.setValue(true);
            } else {
              this.unknownErrorPhoneVerificationControl.setValue(true);
            }
          });
        }
      } else if (this.investorChosenTypeControl.value) {
        userToRegister.userType = UserType.INVESTOR;
        if (this.fullRegistrationCheckboxControl.value) {
          userToRegister.company.name = this.companyNameControl.value;
          userToRegister.company.taxId = this.companyTaxIdControl.value;
          userToRegister.company.form = this.companyFormControl.value;
          userToRegister.company.registeredAddress.city = this.companyCityControl.value;
          userToRegister.company.registeredAddress.flatNumber = this.companyFlatNumberControl.value;
          userToRegister.company.registeredAddress.houseNumber = this.companyCorrespondenceStreetNumberControl.value;
          userToRegister.company.registeredAddress.postalCode = this.companyPostalCodeControl.value;
          userToRegister.company.registeredAddress.street = this.companyStreetNameControl.value;
          if(this.companyCorrespondenceAddressDifferentThenRegistered()) {
            userToRegister.company.correspondenceAddress.city = this.companyCorrespondenceCityControl.value;
            userToRegister.company.correspondenceAddress.flatNumber = this.companyCorrespondenceFlatNumberControl.value;
            userToRegister.company.correspondenceAddress.houseNumber = this.companyCorrespondenceStreetNumberControl.value;
            userToRegister.company.correspondenceAddress.postalCode = this.companyCorrespondencePostalCodeControl.value;
            userToRegister.company.correspondenceAddress.street = this.companyCorrespondenceStreetNameControl.value;
          } else {
            userToRegister.company.correspondenceAddress = userToRegister.company.registeredAddress;
          }
          this.controller.registerFullInvestor(userToRegister, (response: UserRegisterResponse) => {
            if (response == UserRegisterResponse.SUCCESS) {
              this.phoneVerificationSendCompleted = false;
              this.registrationCompleted = true;
            } else if (response == UserRegisterResponse.PHONE_VERIFICATION_CODE_MISMATCH) {
              this.phoneVerificationErrorControl.setValue(true);
            } else {
              this.unknownErrorPhoneVerificationControl.setValue(true);
            }
          });
        } else {
          this.controller.registerBasicInvestor(userToRegister, (response: UserRegisterResponse) => {
            if (response == UserRegisterResponse.SUCCESS) {
              this.phoneVerificationSendCompleted = false;
              this.registrationCompleted = true;
            } else if (response == UserRegisterResponse.PHONE_VERIFICATION_CODE_MISMATCH) {
              this.phoneVerificationErrorControl.setValue(true);
            } else {
              this.unknownErrorPhoneVerificationControl.setValue(true);
            }
          })
        }
      }
    }
  }

}
