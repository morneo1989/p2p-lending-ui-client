"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var router_1 = require("@angular/router");
var users_login_service_1 = require("../services/users.login.service");
var WelcomeNewUserComponent = (function () {
    function WelcomeNewUserComponent(activatedRoute, loginService) {
        this.activatedRoute = activatedRoute;
        this.loginService = loginService;
        this.registrationResponseHeader = 'Proces rejestracji został zakończony';
        this.registrationResponseMessage = 'W celu korzystania z naszej platformy prosimy o zalogowanie do twojego panelu użytkownika';
        console.log('WelcomeNewUserComponent created');
    }
    WelcomeNewUserComponent.prototype.ngOnInit = function () {
        var _this = this;
        console.log('WelcomeNewUserComponent ngOnInit');
        // subscribe to router event
        this.subscription = this.activatedRoute.queryParams.subscribe(function (param) {
            var response = param['response'];
            console.log(response);
            if (response == 'ok') {
                _this.registrationResponseHeader = 'Proces rejestracji został zakończony';
                _this.registrationResponseMessage = 'W celu korzystania z naszej platformy prosimy o zalogowanie do twojego panelu użytkownika';
            }
            else if (response == 'error') {
                _this.registrationResponseHeader = 'Weryfikacja adresu email zakończyła się błędem';
                _this.registrationResponseMessage = 'W trakcie procesu aktywacji adresu email wystąpił błąd';
            }
        });
    };
    WelcomeNewUserComponent.prototype.ngOnDestroy = function () {
        // prevent memory leak by unsubscribing
        this.subscription.unsubscribe();
    };
    WelcomeNewUserComponent = __decorate([
        core_1.Component({
            selector: 'div[welcome-new-user]',
            templateUrl: 'app/templates/welcome-new-user.html'
        }), 
        __metadata('design:paramtypes', [router_1.ActivatedRoute, users_login_service_1.UsersLoginService])
    ], WelcomeNewUserComponent);
    return WelcomeNewUserComponent;
}());
exports.WelcomeNewUserComponent = WelcomeNewUserComponent;
//# sourceMappingURL=welcome-new-user.js.map