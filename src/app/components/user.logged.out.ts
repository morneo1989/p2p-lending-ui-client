import {Component} from "@angular/core";
import {AppLoggerFactory} from "../utilities/app.logger.factory";

@Component({
  selector: 'div[user-login-timeout]',
  templateUrl: '../templates/user-logged-out.html'
})
export class UserLoggedOutComponent {

  private _logger = this._loggerFactory.getLogger('UserLoggedOutComponent');

  constructor(private _loggerFactory: AppLoggerFactory) {
    this._logger.logInfo('component created')
  }

}
