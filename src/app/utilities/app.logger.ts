
import {Logger, Level} from "angular2-logger/core";
export class AppLogger {

  private logRootModule: string;
  private _logger: Logger;

  constructor(logRootModule: string, _logger: Logger) {
    this.logRootModule = logRootModule;
    this._logger = _logger;
    this._logger.Level = Level.LOG;
  }

  public logInfo(info: string) {
    this._logger.info('[' + this.logRootModule + '] ' + info);
  }

  public logWarn(warn: string) {
    this._logger.warn('[' + this.logRootModule + '] ' + warn);
  }

  public logDebug(debug: string) {
    this._logger.debug('[' + this.logRootModule + '] ' + debug);
  }

  public logError(error: string) {
    this._logger.error('[' + this.logRootModule + '] ' + error);
  }

}
