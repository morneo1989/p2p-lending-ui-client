import {Injectable} from "@angular/core";
import {Logger, Level} from "angular2-logger/core";
import {AppLogger} from "./app.logger";
@Injectable()
export class AppLoggerFactory {

  constructor(private _logger: Logger) {
    _logger.level = Level.LOG;
  }

  public getLogger(logRootModule: string): AppLogger {
    return new AppLogger(logRootModule, this._logger);
  }

}
