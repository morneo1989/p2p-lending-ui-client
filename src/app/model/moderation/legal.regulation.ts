export class LegalRegulation {
    public _id: string;
    public title: string;
    public htmlFormattedText: string;

    public constructor(id?: string, title?: string, text?: string) {
        this._id = id;
        this.title = title;
        this.htmlFormattedText = text;
    }

}
