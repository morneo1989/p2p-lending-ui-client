"use strict";
var LegalRegulation = (function () {
    function LegalRegulation(id, title, text) {
        this._id = id;
        this.title = title;
        this.htmlFormattedText = text;
    }
    return LegalRegulation;
}());
exports.LegalRegulation = LegalRegulation;
//# sourceMappingURL=legal.regulation.jss.map
