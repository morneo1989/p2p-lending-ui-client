export class InvestGuide {
    public _id: string;
    public htmlFormattedText: string;

    public constructor(id?: string, text?: string) {
        this._id = id;
        this.htmlFormattedText = text;
    }

}
