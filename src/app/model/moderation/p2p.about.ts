export class P2PAbout {
    public _id: string;
    public htmlFormattedText: string;

    public constructor(id?: string, text?: string) {
        this._id = id;
        this.htmlFormattedText = text;
    }

}
