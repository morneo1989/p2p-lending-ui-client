export class Faq {
    public _id: string;
    public faqQuestion: string;
    public htmlFormattedText: string;

    public constructor(id?: string, text?: string) {
        this._id = id;
        this.htmlFormattedText = text;
    }

}
