import {LegalRegulation} from "../moderation/legal.regulation";
export class LegalRegulationTitlesEvent {
  constructor(public regulationTitles: LegalRegulation[]) {}
}
