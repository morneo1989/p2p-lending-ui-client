import {Auction} from "../auction/auction";
export class LoadedAuctionsEvent {
  constructor(public auctions: Auction[]) {}
}
