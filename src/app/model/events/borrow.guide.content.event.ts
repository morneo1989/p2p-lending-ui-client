import {BorrowGuide} from "../moderation/borrow.guide";
export class BorrowGuideContentEvent {
  constructor(public borrowGuideContent: BorrowGuide) {}
}
