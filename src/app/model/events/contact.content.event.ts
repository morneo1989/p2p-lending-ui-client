import {Contact} from "../moderation/contact";
export class ContactContentEvent {
  constructor(public contactContent: Contact) {}
}
