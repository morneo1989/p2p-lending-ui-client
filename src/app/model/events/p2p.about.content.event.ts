import {P2PAbout} from "../moderation/p2p.about";
export class P2PAboutContentEvent {
  constructor(public p2pAboutContent: P2PAbout) {}
}
