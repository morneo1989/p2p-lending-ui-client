import {Faq} from "../moderation/faq";
export class FaqContentEvent {
  constructor(public faqContent: Faq[]) {}
}
