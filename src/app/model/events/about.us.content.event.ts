import {AboutUs} from "../moderation/about.us";
export class AboutUsContentEvent {
  constructor(public aboutUsContent: AboutUs) {}
}
