import {InvestGuide} from "../moderation/invest.guide";
export class InvestGuideContentEvent {
  constructor(public investGuideContent: InvestGuide) {}
}
