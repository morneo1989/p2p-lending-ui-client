"use strict";
(function (UserRegisterResponse) {
    UserRegisterResponse[UserRegisterResponse["SUCCESS"] = 0] = "SUCCESS";
    UserRegisterResponse[UserRegisterResponse["EMAIL_ALREADY_EXISTS"] = 1] = "EMAIL_ALREADY_EXISTS";
    UserRegisterResponse[UserRegisterResponse["PHONE_VERIFICATION_CODE_MISMATCH"] = 2] = "PHONE_VERIFICATION_CODE_MISMATCH";
    UserRegisterResponse[UserRegisterResponse["UNKNOWN_ERROR"] = 3] = "UNKNOWN_ERROR";
})(exports.UserRegisterResponse || (exports.UserRegisterResponse = {}));
var UserRegisterResponse = exports.UserRegisterResponse;
//# sourceMappingURL=UserRegisterResponse.js.map