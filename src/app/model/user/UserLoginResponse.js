"use strict";
var UserLoginResponse = (function () {
    function UserLoginResponse() {
        this.passwordInvalid = false;
        this.userUnknown = false;
        this.unknownError = false;
        this.emailVerified = false;
    }
    return UserLoginResponse;
}());
exports.UserLoginResponse = UserLoginResponse;
//# sourceMappingURL=UserLoginResponse.js.map