"use strict";
(function (PhoneVerifyResponse) {
    PhoneVerifyResponse[PhoneVerifyResponse["SUCCESS"] = 0] = "SUCCESS";
    PhoneVerifyResponse[PhoneVerifyResponse["EMAIL_ALREADY_EXISTS"] = 1] = "EMAIL_ALREADY_EXISTS";
    PhoneVerifyResponse[PhoneVerifyResponse["UNKNOWN_ERROR"] = 2] = "UNKNOWN_ERROR";
})(exports.PhoneVerifyResponse || (exports.PhoneVerifyResponse = {}));
var PhoneVerifyResponse = exports.PhoneVerifyResponse;
//# sourceMappingURL=PhoneVerifyResponse.js.map