import {UserType} from "./enums/user.type";
export class User {

    public email: string;
    public token: string;
    public password: string;
    public repeatedPassword: string;
    public userType: UserType;
    public name: string;
    public surname: string;
    public phone: string;
    public phoneVerificationCode: string;
    public registeredAddress: {
        street: string,
        flatNumber: string,
        houseNumber: string,
        city: string,
        postalCode: string
    };
    public correspondenceAddress: {
        street: string,
        flatNumber: string,
        houseNumber: string,
        city: string,
        postalCode: string
    };
    public ratingParameters: {
        age: number,
        currentDebt: number,
        employmentArea: string,
        incomeSource: string,
        maritalStatus: string
        maritalStatusPeriodInMonths: number
        monthlyAverageIncome: number
        numberOfPeopleDependent: number
    };
    public company: {
        name: string,
        form: string,
        taxId: string,
        registeredAddress: {
            street: string,
            flatNumber: string,
            houseNumber: string,
            city: string,
            postalCode: string
        },
        correspondenceAddress: {
            street: string,
            flatNumber: string,
            houseNumber: string,
            city: string,
            postalCode: string
        }
    };

    constructor(email?: string, password?: string) {
        this.email = email;
        this.password = password;
        this.registeredAddress = {
            street: null,
            flatNumber: null,
            houseNumber: null,
            city: null,
            postalCode: null
        };
        this.correspondenceAddress = {
            street: null,
            flatNumber: null,
            houseNumber: null,
            city: null,
            postalCode: null
        };
        this.ratingParameters = {
            age: null,
            currentDebt: null,
            employmentArea: null,
            incomeSource: null,
            maritalStatus: null,
            maritalStatusPeriodInMonths: null,
            monthlyAverageIncome: null,
            numberOfPeopleDependent: null
        };
        this.company = {
            name: null,
            form: null,
            taxId: null,
            registeredAddress: {
                street: null,
                flatNumber: null,
                houseNumber: null,
                city: null,
                postalCode: null
            },
            correspondenceAddress: {
                street: null,
                flatNumber: null,
                houseNumber: null,
                city: null,
                postalCode: null
            }
        }
    }

    /*
     * Method used to set all borrower informations considered as extended, and not required at first simple registration process
     * , to be set to null
     * */
    public clearBorrowerExtendedValues(): void {
        this.registeredAddress = {
            street: null,
            flatNumber: null,
            houseNumber: null,
            city: null,
            postalCode: null
        };
        this.correspondenceAddress = {
            street: null,
            flatNumber: null,
            houseNumber: null,
            city: null,
            postalCode: null
        };
        this.ratingParameters = {
            age: null,
            currentDebt: null,
            employmentArea: null,
            incomeSource: null,
            maritalStatus: null,
            maritalStatusPeriodInMonths: null,
            monthlyAverageIncome: null,
            numberOfPeopleDependent: null
        };
    }

    /*
     * Method used to set all borrower informations considered as extended, and not required at first simple registration process
     * , to be set to null
     * */
    public clearInvestorExtendedValues(): void {
        this.company = {
            name: null,
            form: null,
            taxId: null,
            registeredAddress: {
                street: null,
                flatNumber: null,
                houseNumber: null,
                city: null,
                postalCode: null
            },
            correspondenceAddress: {
                street: null,
                flatNumber: null,
                houseNumber: null,
                city: null,
                postalCode: null
            }
        };
    }

}
