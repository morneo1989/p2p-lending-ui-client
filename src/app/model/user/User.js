"use strict";
var UserType_1 = require("./enums/UserType");
var User = (function () {
    function User(email, password) {
        this.email = email;
        this.password = password;
        this.registeredAddress = {
            street: null,
            flatNumber: null,
            houseNumber: null,
            city: null,
            postalCode: null
        };
        this.correspondenceAddress = {
            street: null,
            flatNumber: null,
            houseNumber: null,
            city: null,
            postalCode: null
        };
        this.ratingParameters = {
            age: null,
            currentDebt: null,
            employmentArea: null,
            incomeSource: null,
            maritalStatus: null,
            maritalStatusPeriodInMonths: null,
            monthlyAverageIncome: null,
            numberOfPeopleDependent: null
        };
        this.company = {
            name: null,
            form: null,
            taxId: null,
            registeredAddress: {
                street: null,
                flatNumber: null,
                houseNumber: null,
                city: null,
                postalCode: null
            },
            correspondenceAddress: {
                street: null,
                flatNumber: null,
                houseNumber: null,
                city: null,
                postalCode: null
            }
        };
    }
    User.prototype.setUserType = function (userType) {
        this.userTypeString = userType;
        if (userType == 'INVESTOR') {
            this.userType = UserType_1.UserType.INVESTOR;
            this.userTypeToDisplay = 'Inwestor';
        }
        if (userType == 'BORROWER') {
            this.userType = UserType_1.UserType.BORROWER;
            this.userTypeToDisplay = 'Pożyczkobiorca';
        }
        if (userType == 'ADMIN') {
            this.userType = UserType_1.UserType.ADMIN;
            this.userTypeToDisplay = 'Administrator';
        }
    };
    /*
     * Method used to set all borrower informations considered as extended, and not required at first simple registration process
     * , to be set to null
     * */
    User.prototype.clearBorrowerExtendedValues = function () {
        this.registeredAddress = {
            street: null,
            flatNumber: null,
            houseNumber: null,
            city: null,
            postalCode: null
        };
        this.correspondenceAddress = {
            street: null,
            flatNumber: null,
            houseNumber: null,
            city: null,
            postalCode: null
        };
        this.ratingParameters = {
            age: null,
            currentDebt: null,
            employmentArea: null,
            incomeSource: null,
            maritalStatus: null,
            maritalStatusPeriodInMonths: null,
            monthlyAverageIncome: null,
            numberOfPeopleDependent: null
        };
    };
    /*
     * Method used to set all borrower informations considered as extended, and not required at first simple registration process
     * , to be set to null
     * */
    User.prototype.clearInvestorExtendedValues = function () {
        this.company = {
            name: null,
            form: null,
            taxId: null,
            registeredAddress: {
                street: null,
                flatNumber: null,
                houseNumber: null,
                city: null,
                postalCode: null
            },
            correspondenceAddress: {
                street: null,
                flatNumber: null,
                houseNumber: null,
                city: null,
                postalCode: null
            }
        };
    };
    return User;
}());
exports.User = User;
//# sourceMappingURL=User.js.map