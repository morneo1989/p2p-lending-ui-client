"use strict";
(function (UserType) {
    UserType[UserType["ADMIN"] = 0] = "ADMIN";
    UserType[UserType["INVESTOR"] = 1] = "INVESTOR";
    UserType[UserType["BORROWER"] = 2] = "BORROWER";
})(exports.UserType || (exports.UserType = {}));
var UserType = exports.UserType;
//# sourceMappingURL=UserType.js.map