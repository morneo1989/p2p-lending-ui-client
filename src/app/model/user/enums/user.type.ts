export enum UserType {
    ADMIN = <any>'ADMIN',
    INVESTOR = <any>'INVESTOR',
    BORROWER = <any>'BORROWER'
}
