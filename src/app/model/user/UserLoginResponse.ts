export class UserLoginResponse {

	public name:string;
	public surname:string;
	public userType:string;
	public token:string;
	public emailVerified:boolean;
	public passwordInvalid:boolean;
	public userUnknown:boolean;
	public unknownError:boolean;

	constructor() {
		this.passwordInvalid = false;
		this.userUnknown = false;
		this.unknownError = false;
		this.emailVerified = false;
	}

	public isError(): boolean {
	  return this.passwordInvalid || this.userUnknown || this.unknownError;
  }

}
