export enum PhoneVerifyResponse {
    SUCCESS,
    EMAIL_ALREADY_EXISTS,
    UNKNOWN_ERROR
}