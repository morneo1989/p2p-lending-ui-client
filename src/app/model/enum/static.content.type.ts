export enum StaticContentType {
  FAQ = <any>'FAQ',
  CONTACT = <any>'CONTACT',
  P2P_ABOUT = <any>'P2P_ABOUT',
  INVEST_GUIDE = <any>'INVEST_GUIDE',
  BORROW_GUIDE = <any>'BORROW_GUIDE',
  ABOUT_US = <any>'ABOUT_US',
  LEGAL_REGULATION = <any>'LEGAL_REGULATION'
}
