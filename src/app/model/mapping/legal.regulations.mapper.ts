import {Injectable} from "@angular/core";
import {LegalRegulation} from "../moderation/legal.regulation";
@Injectable()
export class LegalRegulationsMapper {

  public mapFromRestModel(legalRegulationFromRest: any): LegalRegulation {
    let mappedLegalRegulation = new LegalRegulation();
    mappedLegalRegulation._id = legalRegulationFromRest._id;
    mappedLegalRegulation.title = legalRegulationFromRest.legalRegulationTitle;
    mappedLegalRegulation.htmlFormattedText = legalRegulationFromRest.htmlFormattedText;
    return mappedLegalRegulation;
  }

  public mapToRestModel(legalRegulation: LegalRegulation): any {
    return {
      _id: legalRegulation._id,
      legalRegulationTitle: legalRegulation.title,
      htmlFormattedText: legalRegulation.htmlFormattedText
    };
  }

}
