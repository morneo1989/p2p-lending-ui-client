export class Auction {

	public _id: string;
  public title:string;
  public description:string;
  public loanAmount:number;
  public loanTarget:string;
  public rateOfLoan:number;
  public payoffPeriodInMonths:number;

	constructor() {
	}

}
