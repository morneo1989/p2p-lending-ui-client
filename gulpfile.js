var gulp = require('gulp');
var sftp = require("gulp-sftp");
var changed = require("gulp-changed");

gulp.task('deploy-via-sftp', function () {
  gulp.src(['../p2p-lending-ui-client/dist/**'])
    .pipe(changed('./current_server_dist/'))
    .pipe(gulp.dest('./current_server_dist/'))
    .pipe(sftp({
      host: 'ec2-52-29-74-108.eu-central-1.compute.amazonaws.com',
      user: 'ec2-user',
      key: '/home/maciek/.ssh/p2pLendingPlatformSSHKey.pem',
      remotePath: '/pospo_www/development/public'
    }))
});
